package it.unirc.vo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unirc.vo.model.Object;

import java.util.List;
import java.util.Optional;

@Repository
public interface ObjectRepository extends CrudRepository<Object, Long> {
    @Query("SELECT o FROM Object o join fetch o.resources WHERE o.objectId = :objId and o.instanceId = :instId")
    Optional<Object> findByObjectIdAndInstanceId(@Param("objId") String objectId, @Param("instId") String instanceId);

    @Query("SELECT o FROM Object o join fetch o.observers WHERE o.objectId = :objId and o.instanceId = :instId") 
    Optional<Object> findWithObserversByObjectIdAndInstanceId(@Param("objId") String objectId, @Param("instId") String instanceId); //da rimappare(?)

    @Query("SELECT o FROM Object o, Device d WHERE o.objectId = :objId AND o.instanceId = :instId AND o.device.id = d.id AND d.endpoint = :devId")
    Optional<Object> findByEndpointObjectIdInstanceId(@Param("devId") String devId, @Param("objId") String objectId, @Param("instId") String instanceId);

    @Query("SELECT o FROM Object o, Device d WHERE o.objectId = :objId AND o.device.id = d.id AND d.endpoint = :devId")
    List<Object> findByEndpointObjectId(@Param("devId") String devId, @Param("objId") String objectId);
        
    List<Object> findByObjectId(String objectId);
}
