package it.unirc.vo.dto.observe;

import lombok.Data;

@Data
public class ObserveResourceResponse {
    String event;
    ObserveResourceData data;
}
