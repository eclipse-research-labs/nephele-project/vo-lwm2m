package it.unirc.vo.repository;

import com.influxdb.client.DeleteApi;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import com.influxdb.exceptions.InfluxException;
import com.influxdb.spring.influx.InfluxDB2AutoConfiguration;
import com.influxdb.spring.influx.InfluxDB2Properties;
import it.unirc.vo.config.BuilderProvider;
import it.unirc.vo.dto.MqttEventDto;
import it.unirc.vo.dto.MqttPayloadDto;
import it.unirc.vo.model.Event;
import it.unirc.vo.model.ValueInflux;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Component("InfluxFunctions")
public class InfluxFunctions {

    private InfluxDB2AutoConfiguration influxDB2AutoConfiguration;
    private InfluxDBClient influxDBClient;
    private InfluxDB2Properties properties;

    private BuilderProvider builderProvider;


    @Autowired
    public InfluxFunctions(InfluxDB2AutoConfiguration influxDB2AutoConfiguration, InfluxDB2Properties properties, BuilderProvider builderProvider) {
        this.influxDB2AutoConfiguration = influxDB2AutoConfiguration;
        this.influxDBClient = influxDB2AutoConfiguration.influxDBClient();
        this.properties = properties;
        this.builderProvider = builderProvider;

    }

    // method used to write a single row on db.
    // Input: InfluxDBClient class object used to perform a write operation, Event
    // class object used to store JSON string
    public boolean writeSinglePoint(Event event, MqttPayloadDto payload, Path path) {
        boolean result = false;
        try {

            WriteApiBlocking writeApi = influxDBClient.getWriteApiBlocking();

            CharSequence tmstp = payload.getTmstp(); // timestamp

            // point creation. The data structure is formed using measurement() and
            // addField() methods
            // .time() method disabled for testing purposes
            Point point = Point.measurement("event").addField("value", event.getSource());// .time(Instant.parse(tmstp),
                                                                                          // WritePrecision.MS);

            // writing instruction
            writeApi.writePoint(point);

            result = true;
        } catch (InfluxException e) {
            System.out.println("Exception!!" + e.getMessage());
        }
        return result;
    }


    public boolean writePointValue (Long resourceID, String value, String type) {
        boolean result = false;
        try {
            WriteApiBlocking writeApi = influxDBClient.getWriteApiBlocking();
            Point p = null;

            //TODO Set condition: if timestamp is not null, save it. Else generate through InfluxDB.

            //Write point è più performante della scrittura tramite l'utilizzo del Pojo che causava perdita di informazioni


            //TODO Oltre allo switch, si potrebbe implementare una misura cautelare per quanto riguarda il matching tra il campo Type ed il valore effettivo inviato.
            boolean goodType = false; //I use this value to not store the message in case of bad type.
            switch (type){
                case "String":
                    p = Point.measurement(
                        "Value").addTag("resourceID", resourceID.toString()).addTag("type", type).addField("valueString", value);// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    goodType = true;
                    break;

                case "Float":
                    Float valueFloat;
                    try {
                        valueFloat = Float.parseFloat(value);
                    } catch (Exception e) {
                        System.out.println("Exception in parsing Float values: " + e.getMessage());
                        break;
                    }
                    p = Point.measurement(
                        "Value").addTag("resourceID", resourceID.toString()).addTag("type", type).addField("valueFloat", valueFloat);// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    goodType = true;
                    break;

                case "Time":
                    p = Point.measurement(
                        "Value").addTag("resourceID", resourceID.toString()).addTag("type", type).addField("valueTime", value);// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    goodType = true;
                    break;

                case "Integer":
                    int valueInteger;
                    try {
                        valueInteger = Integer.parseInt(value);
                    } catch (Exception e) {
                        System.out.println("Exception in parsing Integer values: " + e.getMessage());
                        break;
                    }
                    p = Point.measurement(
                        "Value").addTag("resourceID", resourceID.toString()).addTag("type", type).addField("valueInteger", valueInteger);// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    goodType = true;
                    break;

                case "Boolean":
                    boolean valueBoolean;
                    try {
                        valueBoolean = Boolean.parseBoolean(value);
                        } catch (Exception e) {
                        System.out.println("Exception in parsing Float values: " + e.getMessage());
                        break;
                    }
                    p = Point.measurement(
                        "Value").addTag("resourceID", resourceID.toString()).addTag("type", type).addField("valueBoolean", valueBoolean);// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    goodType = true;
                    break;

                default:
                    p = Point.measurement(
                        "Value").addTag("resourceID", resourceID.toString()).addTag("type", type).addField("valueDefault", value);// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    goodType = true;
                    break;
            }
            if (goodType) {
                writeApi.writePoint(p);
            }
            result = true;

        } catch (InfluxException e) {
            System.out.println("Exception!!" + e.getMessage());
        }
        return result;

    }



    // method used to write a set of rows on db.
    // Input: InfluxDBClient class object used to perform a write operation,
    // MqttPayloadDto class object used to obtain single resource values
    public boolean writeMultiplePoints(MqttPayloadDto payload, Path path) {
        boolean result = false;
        try {
            WriteApiBlocking writeApi = influxDBClient.getWriteApiBlocking();
            List<Point> listPoint = new ArrayList<Point>();
            CharSequence tmstp = payload.getTmstp(); // timestamp
            List<MqttEventDto> list_events = payload.getE(); // list of single resource updates
            String alphaRegex = ".*[A-Za-z\\s].*"; // RegEx for alphabetic characters and white space
            String numRegex = ".*[0-9].*"; // RegEx for numbers

            //List<ValueInflux> valueList = new ArrayList<ValueInflux>();

            for (MqttEventDto ev : list_events) {


                if (ev.getV().matches(alphaRegex)) { // if the event value is a string


                    Point p = Point.measurement(
                            "Value").addTag("resourceId", ev.getN()).addField("value", ev.getV());// .time(Instant.parse(tmstp),
                                                                                                        // WritePrecision.MS);




                    listPoint.add(p);

                    /*
                    ValueInflux value = new ValueInflux();
                    value.setResourceId(ev.getN());
                    value.setValue(ev.getV());
                    valueList.add(value);

                     */


                } else if (ev.getV().matches(numRegex)) { // if the event value is a numeric value

                    Point p = Point.measurement(
                            "Value").addTag("resourceId", ev.getN()).addField("value", Float.parseFloat(ev.getV()));// .time(Instant.parse(tmstp),
                    // WritePrecision.MS);
                    listPoint.add(p);

                    /*
                    ValueInflux value = new ValueInflux();
                    value.setResourceId(ev.getN());
                    value.setValue(ev.getV());
                    valueList.add(value);

                     */


                }
            }

            /*
            writeApi.writeMeasurements(WritePrecision.MS, valueList);

             */


            writeApi.writePoints(listPoint);



            // signifies write is done successfully
            result = true;
        } catch (InfluxException e) {
            System.out.println("Exception!!" + e.getMessage());
        }
        return result;
    }

    public boolean writeValue(ValueInflux value) {
        boolean flag = false;

        try {
            WriteApiBlocking writeApi = influxDBClient.getWriteApiBlocking();
            writeApi.writeMeasurement(WritePrecision.MS, value);

            flag = true;

        } catch (InfluxException e) {
            System.out.println("Exception!!" + e.getMessage());
        }

        return flag;

    }

    // DA QUI IN GIÙ
    // check time window:
    public boolean intervalCheck(String start_time, String stop_time) {
        OffsetDateTime start = OffsetDateTime.parse(start_time);
        OffsetDateTime stop = OffsetDateTime.parse(stop_time);
        if (!start.isEqual(stop)) {
            if (start.isBefore(stop)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }


    //TODO implementare TYPE anche qui.
    public List<ValueInflux> queryAllDataMod(String resourceID) {

        String flux = String.format(
                "from(bucket:\"%s\") " +
                        "|> range(start:0) " +
                        "|> filter(fn: (r) => r[\"_measurement\"] == \"Value\") " +
                        "|> filter(fn: (r) => r[\"resourceID\"] == \"%s\") " +
                        "|> sort(columns: [\"_time\"], desc: true) " +
                        //"|> yield(name: \"sort\")" +
                        "|> pivot(rowKey:[\"_time\"], columnKey: [\"_field\"], valueColumn: \"_value\")",
                properties.getBucket(), resourceID);

        QueryApi queryApi = influxDBClient.getQueryApi();

        List<ValueInflux> values = queryApi.query(flux, ValueInflux.class);

        return values;
    }

    public ValueInflux getLastValueUpdate(String resourceID) {
        List<ValueInflux> allValue = queryAllDataMod(resourceID);
        if (allValue.isEmpty())
            return null;

        return allValue.get(0);
    }

    // Returns all data stored in the indicated measurement
    public List<ValueInflux> queryAllData(InfluxDBClient influxDBClient, String bucket, String start, String stop,
            String measurement) {
        String flux = "from(bucket:" + "\"" + bucket + "\") |> range(start: " + start + ", stop: " + stop
                + ") |> filter(fn: (r) => r[\"_measurement\"] == " + "\"" + measurement
                + "\") |> filter(fn: (r) => r[\"_field\"] == \"value\")";

        List<ValueInflux> datas = null;
        QueryApi queryApi = influxDBClient.getQueryApi();
        try {
            if (intervalCheck(start, stop)) {
                datas = queryApi.query(flux, ValueInflux.class);
            } else {
                System.out.println("Invalid interval");
            }
        } catch (Exception e) {
            System.out.println("Eccezione");
        }

        if (datas.isEmpty())// DA ECCEZIONE SE data è NULL, la soluzione è non fare controllo e restituire
                            // datas direttamente. L'ho lasciato perchè non ho testato
            return null;
        else
            return datas;

        // return datas;
    }

    // Returns data stored in the indicated measurement with a particular timestamp
    // (ID)
    public ValueInflux queryDataByTime(InfluxDBClient influxDBClient, String bucket, String start, String stop,
            String measurement, String timestamp) {
        String flux = "from(bucket:" + "\"" + bucket + "\") |> range(start: " + start + ", stop: " + stop
                + ") |> filter(fn: (r) => r[\"_measurement\"] == " + "\"" + measurement
                + "\") |> filter(fn: (r) => r[\"_field\"] == \"value\") |> filter(fn: (r) => r[\"_time\"] == "
                + timestamp + ")";

        List<ValueInflux> datas = null;
        QueryApi queryApi = influxDBClient.getQueryApi();

        try {
            if (intervalCheck(start, stop)) {
                datas = queryApi.query(flux, ValueInflux.class);
            } else {
                System.out.println("Invalid interval");
            }
        } catch (Exception e) {
            System.out.println("Eccezione!");
        }

        if (datas.isEmpty())// DA ECCEZIONE SE data è NULL, la soluzione è non fare controllo e restituire
                            // datas direttamente. L'ho lasciato perchè non ho testato
            return null;
        else
            return datas.get(0);

    }

    // deletes indicated measurement data using a time window [start-stop]
    public boolean deleteRecord(InfluxDBClient influxDBClient, String organization, String bucket, String measurement,
            String start_time, String stop_time) {
        boolean flag = false;
        DeleteApi deleteApi = influxDBClient.getDeleteApi();

        try {

            OffsetDateTime start = OffsetDateTime.parse(start_time);
            OffsetDateTime stop = OffsetDateTime.parse(stop_time);
            if (!start.isEqual(stop)) {
                if (start.isBefore(stop)) {
                    // String predicate = "_measurement=\"sensor\" AND sensor_id = \"TLM0201\"";
                    String predicate = "_measurement=\"" + measurement + "\"";
                    deleteApi.delete(start, stop, predicate, bucket, organization);
                    flag = true;
                } else {
                    return flag;
                }

            } else {
                return false;
            }

        } catch (InfluxException ie) {
            System.out.println("InfluxException: " + ie);
        }
        return flag;
    }

    // Returns all data stored in the indicated measurement order by _time DESC
    public List<ValueInflux> findByResourceId(InfluxDBClient influxDBClient, String bucket, String measurement,
            String start, String stop) {
        String flux = "from(bucket: " + "\"" + bucket + "\")|> range(start: " + start + ", stop: " + stop
                + ") |> filter(fn: (r) => r[\"_measurement\"] == " + "\"" + measurement
                + "\") |> sort(columns: [\"_time\"], desc: true)";

        List<ValueInflux> datas = null;
        QueryApi queryApi = influxDBClient.getQueryApi();
        try {
            if (intervalCheck(start, stop)) {
                datas = queryApi.query(flux, ValueInflux.class);
            } else {
                System.out.println("Invalid interval");
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return datas;
    }

    // Returns all data lower than the threshold stored in the indicated
    // measurement, order by _time DESC
    public List<ValueInflux> findByResourceIdAndValueLessThanOrderByCreatedAtDesc(InfluxDBClient influxDBClient,
            String bucket, String measurement, double threshold, String start_time, String stop_time) {
        String thresh = String.valueOf(threshold);
        String flux = "from(bucket: " + "\"" + bucket + "\")|> range(start: " + start_time + ", stop: " + stop_time
                + ") |> filter(fn: (r) => r[\"_measurement\"] == " + "\"" + measurement
                + "\") |> filter(fn: (r) => r._value < " + thresh + ")|> sort(columns: [\"_time\"], desc: true)";
        List<ValueInflux> datas = null;
        QueryApi queryApi = influxDBClient.getQueryApi();
        try {
            if (intervalCheck(start_time, stop_time)) {
                datas = queryApi.query(flux, ValueInflux.class);
            } else {
                System.out.println("Invalid interval");
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return datas;
    }

    // Returns all data bigger than the threshold stored in the indicated
    // measurement, order by _time DESC
    public List<ValueInflux> findByResourceIdAndValueMoreThanOrderByCreatedAtDesc(InfluxDBClient influxDBClient,
            String bucket, String measurement, double threshold, String start_time, String stop_time) {
        String thresh = String.valueOf(threshold);
        String flux = "from(bucket: " + "\"" + bucket + "\")|> range(start: " + start_time + ", stop: " + stop_time
                + ") |> filter(fn: (r) => r[\"_measurement\"] == " + "\"" + measurement
                + "\") |> filter(fn: (r) => r._value > " + thresh + ")|> sort(columns: [\"_time\"], desc: true)";
        List<ValueInflux> datas = null;
        QueryApi queryApi = influxDBClient.getQueryApi();
        try {
            if (intervalCheck(start_time, stop_time)) {
                datas = queryApi.query(flux, ValueInflux.class);
            } else {
                System.out.println("Invalid interval");
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return datas;
    }

    // Returns last value stored in the indicated measurement
    public ValueInflux lastValue(InfluxDBClient influxDBClient, String bucket, String measurement, String start,
            String stop) {
        String flux = "from(bucket: " + "\"" + bucket + "\")|> range(start: " + start + ", stop: " + stop
                + ") |> filter(fn: (r) => r[\"_measurement\"] == " + "\"" + measurement
                + "\") |> filter(fn: (r) => r[\"_field\"] == \"value\") |> last()";
        List<ValueInflux> datas = null;
        QueryApi queryApi = influxDBClient.getQueryApi();

        try {
            if (intervalCheck(start, stop)) {
                datas = queryApi.query(flux, ValueInflux.class);
            } else {
                System.out.println("Invalid interval");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        if (datas.isEmpty())
            return null;
        else
            return datas.get(0);

    }// fare riferimeno al POJO per accesso ai info (es. data.measurement,
     // data.value, data.time)

    public InfluxDBClient getInfluxDBClient() {
        return influxDBClient;
    }

    public InfluxDB2Properties getProperties() {
        return properties;
    }

}
