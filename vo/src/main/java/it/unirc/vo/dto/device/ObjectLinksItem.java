package it.unirc.vo.dto.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObjectLinksItem {

    @JsonProperty("url")
    private String url;

    @JsonProperty("attributes")
    private Attributes attributes;

}
