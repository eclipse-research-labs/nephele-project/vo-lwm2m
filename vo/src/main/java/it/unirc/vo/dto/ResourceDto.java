package it.unirc.vo.dto;

import lombok.Data;

@Data
public class ResourceDto {
    String id;
    String value;
    String timestamp;
}
