package it.unirc.vo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.unirc.vo.dto.ResourceDto;
import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveResourceData;
import it.unirc.vo.dto.observe.ObserveResourceResponse;
import it.unirc.vo.model.Object;
import it.unirc.vo.model.*;
import it.unirc.vo.repository.*;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@Transactional
public class ObserverService {
    private final ObservableRepository observableRepository;
    private final ObserverRepository observerRepository;
    private final ValueRepository valueRepository;
    private final ResourceRepository resourceRepository;
    private final ObjectMapper objectMapper;

    private final InfluxFunctions influxFunctions;

    @Autowired
    public ObserverService(ObservableRepository observableRepository,
            ObserverRepository observerRepository,
            ValueRepository valueRepository,
            ResourceRepository resourceRepository,
            ObjectMapper objectMapper, InfluxFunctions influxFunctions) {
        this.observableRepository = observableRepository;
        this.observerRepository = observerRepository;
        this.valueRepository = valueRepository;
        this.objectMapper = objectMapper;
        this.resourceRepository = resourceRepository;
        this.influxFunctions = influxFunctions;
    }

    public List<Observer> getObserversByObservableId(Long id) {
        Observable observable = observableRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(id.toString()));
        return observable.getObservers();
    }

    public void sendResourceNotification(Observer observer, Resource resource) throws IOException {
        log.info("Sending notification to: " + observer.getAddress() + " for resource: "
                + resource.getObject().getObjectId() + "/" + resource.getObject().getInstanceId() + "/"
                + resource.getResourceId());

        try {
            ObserveResourceResponse response = new ObserveResourceResponse();
            response.setEvent("NOTIFICATION");
            ObserveResourceData data = new ObserveResourceData();
            data.setEp(resource.getObject().getDevice().getEndpoint());
            data.setRes("/" + resource.getObject().getObjectId() + "/" + resource.getObject().getInstanceId() + "/"
                    + resource.getResourceId());
            ResourceDto resourceDto = new ResourceDto();
            resourceDto.setId(resource.getResourceId());

            // OLD METHOD
            /*
             * valueRepository.findFirstByResourceIdOrderByCreatedAtDesc(resource.getId()).
             * ifPresent(value -> {
             * resourceDto.setValue(value.getValue());
             * resourceDto.setTimestamp(value.getCreatedAt().toString());
             * });
             */

            // NEW INFLUX METHOD
            ValueInflux value = influxFunctions.getLastValueUpdate(resource.getId().toString());
            if (value != null) {

                resourceDto.setValue(value.getValue().toString());
                resourceDto.setTimestamp(value.getTime().toString());
            } else {
                log.info("Value is null");
            }

            data.setVal(resourceDto);
            response.setData(data);

            this.sendNotification(observer.getAddress(), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (observer.isOneShot())
            observerRepository.delete(observer);
    }

    public void sendResourceNotification(Observer observer, ObserveResourceResponse response) throws IOException {
        log.info("Sending notification to: " + observer.getAddress() + " for resource: " + response.getData().getRes());
        this.sendNotification(observer.getAddress(), response);

        if (observer.isOneShot())
            observerRepository.delete(observer);
    }

    // DISABILITATO AL MOMENTO
    public void sendObjectInstanceNotification(Observer observer, Object object) throws IOException {
        log.info("Disabled at the moment.");
        /*
        log.info("Sending notification to: " + observer.getAddress() +
                "For Instance: " + object.getObjectId() + "/" + object.getInstanceId());

        try {
            ObserveObjectResponse response = new ObserveObjectResponse();
            response.setEvent("NOTIFICATION");
            ObserveObjectData data = new ObserveObjectData();
            data.setEp(object.getDevice().getEndpoint());
            data.setRes("/" + object.getObjectId() + "/" + object.getInstanceId());
            ObserveObjectVal val = new ObserveObjectVal();
            val.setId(object.getInstanceId());
            List<ResourceDto> resources = new ArrayList<>();
            resourceRepository
                    .findResourceByDeviceObjectInstanceId(object.getDevice().getEndpoint(),
                            object.getObjectId(), object.getInstanceId())
                    .forEach(resource -> {
                        log.info(resource.id.toString());
                        ResourceDto resourceDto = new ResourceDto();
                        resourceDto.setId(resource.getResourceId());

                        // OLD METHOD

                         * resource.getValues().stream().findFirst().ifPresent(value -> {
                         * resourceDto.setValue(value.getValue());
                         * resourceDto.setTimestamp(value.getCreatedAt().toString());
                         * });


                        // NEW METHOD
                        ValueInflux lastValue = influxFunctions.getLastValueUpdate(resource.getId().toString());
                        resourceDto.setValue(lastValue.getValue().toString());
                        resourceDto.setTimestamp(lastValue.getTime().toString());

                        resources.add(resourceDto);
                    });
            val.setResources(resources);
            data.setVal(val);
            response.setData(data);

            this.sendNotification(observer.getAddress(), response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (observer.isOneShot())
            observerRepository.delete(observer);
    */
    }

    public void sendObjectInstanceNotification(Observer observer, ObserveObjectResponse response) throws IOException {
        log.info("Disabled at the moment.");
        /*
        log.info("Sending notification to: " +
                observer.getAddress() + " for res: " + response.getData().getRes());
        this.sendNotification(observer.getAddress(), response);

        if (observer.isOneShot())
            observerRepository.delete(observer);
        */
    }

    public void sendNotification(String address, java.lang.Object body) {

        log.info("Sending notification to address: " + address);
        log.info("Object body: "+ body.toString());
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit retrofit = new Retrofit.Builder()
                // .baseUrl("http://" + address + ":8080")
                .baseUrl(address)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();

        NotificationService service = retrofit.create(NotificationService.class);
        // TODO: Implement Async Call
        /*
         * Call<ResponseBody> callAsync = service.sendNotify(body);
         * callAsync.enqueue(new Callback<ResponseBody>(){
         *
         * @Override
         * public void onResponse(Call<UserApiResponse> call, Response<UserApiResponse>
         * response)
         * {
         * if (response.isSuccessful())
         * {
         * UserApiResponse apiResponse = response.body();
         *
         * //API response
         * System.out.println(apiResponse);
         * }
         * else
         * {
         * System.out.println("Request Error :: " + response.errorBody());
         * }
         * }
         *
         * @Override
         * public void onFailure(Call<UserApiResponse> call, Throwable t)
         * {
         * System.out.println("Network Error :: " + t.getLocalizedMessage());
         * }
         * });
         */

        Call<ResponseBody> callSync = service.sendNotify(body);
        try {
            Response<ResponseBody> r = callSync.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
