package it.unirc.vo.selection;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.stereotype.Service;

import it.unirc.vo.config.RegistrationConfig;
import lombok.extern.slf4j.Slf4j;
@Slf4j

//@Service
public class CoAPCondition implements Condition {

    	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {

    		//log.info(String.valueOf(context.getEnvironment().getProperty("vo.device.bindingMode")));// toString());
    		 return context.getEnvironment().getProperty("vo.device.bindingMode").contains("U");

    	}
}