package it.unirc.vo.service;

import it.unirc.vo.dto.ObjectInstanceDto;
import it.unirc.vo.dto.ResourceDto;
import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveResourceResponse;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface CompositeService {
    @GET("/api/clients/{deviceId}")
    public Call<DeviceResponse> getUser(@Path("deviceId") String deviceId);

    @POST("/api/clients/{deviceId}/{objectId}/{instanceId}/{resourceId}")
    public Call<ResponseBody> postCommand(
            @Path("deviceId") String deviceId,
            @Path("objectId") String objectId,
            @Path("instanceId") String instanceId,
            @Path("resourceId") String resourceId
    );

    @PUT("/api/clients/{endpoint}/{objectId}/{instanceId}")
    public Call<ResponseBody> putObjectInstance(
            @Path("endpoint") String endpoint,
            @Path("objectId") String objectId,
            @Path("instanceId") String instanceId,
            @Body ObjectInstanceDto objectInstanceDto
    );

    @PUT("/api/clients/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public Call<ResponseBody> putResource(
            @Path("endpoint") String endpoint,
            @Path("objectId") String objectId,
            @Path("instanceId") String instanceId,
            @Path("resourceId") String resourceId,
            @Body ResourceDto resourceDto
    );

    @POST("/api/clients/{endpoint}/{objectId}/{instanceId}/observe")
    public Call<ObserveObjectResponse> observeObjectInstance(
            @Path("endpoint") String endpoint,
            @Path("objectId") String objectId,
            @Path("instanceId") String instanceId);

    @POST("/api/clients/{endpoint}/{objectId}/{instanceId}/{resourceId}/observe")
    public Call<ObserveResourceResponse> observeResource(
            @Path("endpoint") String endpoint,
            @Path("objectId") String objectId,
            @Path("instanceId") String instanceId,
            @Path("resourceId") String resourceId);

    @POST("/api/cvo/register")
    public Call<ResponseBody> registerCvo(@Body RequestBody endpIdService);

}