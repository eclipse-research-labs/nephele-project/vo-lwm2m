package it.unirc.vo.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Slf4j
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "composite-vo")
public class CompositeConfig {
    private String endpoint;
    private List<Map<String, Object>> devices;
}
