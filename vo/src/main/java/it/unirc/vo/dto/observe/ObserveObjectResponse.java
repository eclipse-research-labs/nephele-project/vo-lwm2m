package it.unirc.vo.dto.observe;

import lombok.Data;

@Data
public class ObserveObjectResponse {
    String event;
    ObserveObjectData data;
}
