package it.unirc.vo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.model.Device;
import it.unirc.vo.repository.DeviceRepository;

import java.util.Optional;

@Service
public class DeviceService {
    private final DeviceRepository deviceRepository;

    public DeviceService(@Autowired DeviceRepository deviceRepository){
        this.deviceRepository = deviceRepository;
    }

    public Optional<Device> findByEndpoint(String endpoint) {
        return deviceRepository.findByEndpoint(endpoint);
    }

    public Device create(Device device) {
        return deviceRepository.save(device);
    }

    public DeviceResponse getDeviceResponseFromEndpoint(String endpoint) {
        return null;
    }
}
