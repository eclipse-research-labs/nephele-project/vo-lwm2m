package it.unirc.vo.dto;

import lombok.Data;

import java.util.List;

@Data
public class ContentObjectDto {
    String id;
    List<ObjectInstanceDto> instances;
}
