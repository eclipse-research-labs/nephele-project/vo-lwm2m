FROM maven:eclipse-temurin as builder
COPY vo vo
RUN mvn -f vo/pom.xml clean install

FROM eclipse-temurin:17-jdk-alpine
COPY --from=builder vo/target/*.jar usr/app/app.jar
#COPY config/application-registration.yaml application-registration.yaml
EXPOSE 8080
ENTRYPOINT ["java","-jar","usr/app/app.jar"]