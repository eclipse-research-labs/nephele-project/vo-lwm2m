package it.unirc.vo.dto.xml;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import it.unirc.vo.model.Resource;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class Resourcexml {
    @JsonProperty("ID")
    String ID;
    String Name;
    Resource.Operations Operations;
    String MultipleInstances;
    String Mandatory;
    String Type;
    String RangeEnumeration;
    String Units;
    String Description;
}
