package it.unirc.vo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.unirc.vo.model.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
}
