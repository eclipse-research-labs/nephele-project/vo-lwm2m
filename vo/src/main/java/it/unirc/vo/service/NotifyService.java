package it.unirc.vo.service;

import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveResourceResponse;
import it.unirc.vo.repository.InfluxFunctions;
import it.unirc.vo.repository.ObjectRepository;
import it.unirc.vo.repository.ResourceRepository;
import it.unirc.vo.repository.ValueRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Log4j2
@Service
public class NotifyService {
    private final ObjectRepository objectRepository;
    private final ResourceRepository resourceRepository;
    private final ObserverService observerService;
    private final ValueRepository valueRepository;

    private final InfluxFunctions influxFunctions;

    @Autowired
    public NotifyService(ObjectRepository objectRepository,
            ObserverService observerService,
            ResourceRepository resourceRepository,
            ValueRepository valueRepository,
            InfluxFunctions influxFunctions) {
        this.objectRepository = objectRepository;
        this.observerService = observerService;
        this.resourceRepository = resourceRepository;
        this.valueRepository = valueRepository;

        this.influxFunctions = influxFunctions;
    }

    public void notifyResource(ObserveResourceResponse resourceResponse) {
        String[] result = resourceResponse.getData().getRes().split("/");
        String objId = result[1];
        String instId = result[2];
        String resId = result[3];

        this.resourceRepository
                .findResourceByDeviceObjectInstanceResourceId(resourceResponse.getData().getEp(), objId, instId, resId)
                .ifPresent(resource -> {
                    // Save Value just arrived

                    // OLD METHOD
                    /*
                     * Value value = new Value();
                     * value.setValue(resourceResponse.getData().getVal().getValue());
                     * value.setResource(resource);
                     * valueRepository.save(value);
                     */

                    // NEW INFLUX METHOD
                    //ValueInflux value = new ValueInflux();
                    //value.setValue(resourceResponse.getData().getVal().getValue());
                    //value.setResourceId(resource.id);
                    //influxFunctions.writeValue(value);

                    influxFunctions.writePointValue(resource.id, resourceResponse.getData().getVal().getValue(), resource.type);
                    log.info("Does the resource " + resource.getResourceId() + " have observers?" + !resource.getObservers().isEmpty());
                    // Send Notification to observers
                    resource.getObservers().forEach(observer -> { log.info("Observer found for resource: " + resource.getResourceId() + " at address: " + observer.getAddress());
                        try {
                            log.info("Try to send notification to: " + observer.getAddress() + " for the resource: " + resource.getResourceId() );
                            observerService.sendResourceNotification(observer, resourceResponse);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                });
    }

    public void notifyObject(ObserveObjectResponse objectResponse) {
        String[] result = objectResponse.getData().getRes().split("/");
        String objId = result[1];
        String instId = result[2];

        // Save Value just arrived
        objectResponse.getData().getVal().getResources().forEach(resourceDto -> {
            this.resourceRepository
                    .findResourceByDeviceObjectInstanceResourceId(objectResponse.getData().getEp(), objId, instId,
                            resourceDto.getId())
                    .ifPresent(resource -> {

                        // OLD METHOD
                        /*
                         * Value value = new Value();
                         * value.setValue(resourceDto.getValue());
                         * value.setResource(resource);
                         * valueRepository.save(value);
                         */

                        //ValueInflux value = new ValueInflux();
                        //value.setValue(resourceDto.getValue());
                        //value.setResourceId(resource.id);
                        //influxFunctions.writeValue(value);

                        influxFunctions.writePointValue(resource.id, resourceDto.getValue(),resource.type);
                    });
        });

        // Send Notification to observers
        this.objectRepository
                .findByEndpointObjectIdInstanceId(objectResponse.getData().getEp(), objId, instId)
                .ifPresent(object -> {
                    object.getObservers().forEach(observer -> {
                        try {
                            observerService.sendObjectInstanceNotification(observer, objectResponse);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                });
    }

}
