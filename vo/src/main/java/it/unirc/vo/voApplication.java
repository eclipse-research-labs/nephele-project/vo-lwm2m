package it.unirc.vo;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

//import java.io.File;
import java.net.BindException;
//import java.net.URI;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import it.unirc.vo.mqtt.MqClient;

@SpringBootApplication
@EntityScan(basePackages = {"it.unirc.vo.model"} )
@EnableJpaRepositories(basePackages = {"it.unirc.vo.repository"})
public class voApplication {

    static {
        // Define a default logback.configurationFile
        String property = System.getProperty("logback.configurationFile");
        if (property == null) {
            System.setProperty("logback.configurationFile", "logback-config.xml");
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(voApplication.class);

    private final static String[] modelPaths = new String[] {

            "10241.xml", "10242.xml", "10243.xml", "10244.xml", "10245.xml", "10246.xml", "10247.xml", "10248.xml",
            "10249.xml", "10250.xml",

            "2048.xml", "2049.xml", "2050.xml", "2051.xml", "2052.xml", "2053.xml", "2054.xml", "2055.xml", "2056.xml",
            "2057.xml",

            "3200.xml", "3201.xml", "3202.xml", "3203.xml", "3300.xml", "3301.xml", "3302.xml", "3303.xml", "3304.xml",
            "3305.xml", "3306.xml", "3308.xml", "3310.xml", "3311.xml", "3312.xml", "3313.xml", "3314.xml", "3315.xml",
            "3316.xml", "3317.xml", "3318.xml", "3319.xml", "3320.xml", "3321.xml", "3322.xml", "3323.xml", "3324.xml",
            "3325.xml", "3326.xml", "3327.xml", "3328.xml", "3329.xml", "3330.xml", "3331.xml", "3332.xml", "3333.xml",
            "3334.xml", "3335.xml", "3336.xml", "3337.xml", "3338.xml", "3339.xml", "3340.xml", "3341.xml", "3342.xml",
            "3343.xml", "3344.xml", "3345.xml", "3346.xml", "3347.xml", "3348.xml", "3349.xml", "3350.xml",

            "Communication_Characteristics-V1_0.xml",

            "LWM2M_Lock_and_Wipe-V1_0.xml", "LWM2M_Cellular_connectivity-v1_0.xml",
            "LWM2M_APN_connection_profile-v1_0.xml", "LWM2M_WLAN_connectivity4-v1_0.xml",
            "LWM2M_Bearer_selection-v1_0.xml", "LWM2M_Portfolio-v1_0.xml", "LWM2M_DevCapMgmt-v1_0.xml",
            "LWM2M_Software_Component-v1_0.xml", "LWM2M_Software_Management-v1_0.xml",

            "Non-Access_Stratum_NAS_configuration-V1_0.xml" };

    private final static String USAGE = "java -jar vo.jar [OPTION]\n\n";

    // private final static String DEFAULT_KEYSTORE_ALIAS = "vo";

    public static void main(String[] args) {
        // Define options for command line tools
        Options options = new Options();

        options.addOption("h", "help", false, "Display help information.");
        options.addOption("brh", "brokerhost", true, "Set the remote Broker address");
        options.addOption("brp", "brokerport", true, "Set the remote Broker address");
        options.addOption("mqh", "mqtthost", true, "Set the local mqtt address.\n  Default: any local address.");
        options.addOption("mqp", "mqttport", true, String.format("Set the local mqtt port.\n  Default: 8883."));
        options.addOption("mqos", "mqttqos", true, "Set the mqtt qos.\n Default: 0.");
        options.addOption("mqusr", "mqttuser", true, "Set the mqtt user for the Client.\n Default: user.");
        options.addOption("mqpsw", "mqttpassword", true, "Set the mqtt password.\n Default: password.");
        options.addOption("address", "webhost", true,
                "Set the HTTP address for web server.\n Default: any local address.");
        options.addOption("port", "webport", true, "Set the HTTP port for web server.\nDefault: 8080.");
        options.addOption("m", "modelsfolder", true, "A folder which contains object models in OMA DDF(.xml) format.");
        options.addOption("sql", "sqlite", true, "Set the location of the SQLite database");
        options.addOption("mdns", "publishDNSSdServices", false, "Publish VO's services to DNS Service discovery");
        // options.addOption("clntId", "clientId", true, "Define client Id");
        // add clean session

        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(120);
        formatter.setOptionComparator(null);

        // Parse arguments
        CommandLine cl;
        try {
            cl = new DefaultParser().parse(options, args);
        } catch (ParseException e) {
            System.err.println("Parsing failed.  Reason: " + e.getMessage());
            formatter.printHelp(USAGE, options);
            return;
        }

        // Print help
        if (cl.hasOption("help")) {
            formatter.printHelp(USAGE, options);
            return;
        }

        // Abort if unexpected options
        if (cl.getArgs().length > 0) {
            System.err.println("Unexpected option or arguments : " + cl.getArgList());
            formatter.printHelp(USAGE, options);
            return;
        }

        // get local address for MQTT services
        String localAddress = cl.getOptionValue("mqh");
        String localPortOption = cl.getOptionValue("mqp");
        int localPort = 1883;
        if (localPortOption != null) {
            localPort = Integer.parseInt(localPortOption);
        }

        // get Broker adddress
        String brokerAddr = cl.getOptionValue("brh");
        String brokerPortOption = cl.getOptionValue("brp");
        int brokerPort = 1883;
        if (brokerPortOption != null) {
            brokerPort = Integer.parseInt(brokerPortOption);
        }

        // Mqtt QoS for subscription
        String mqttQosOpt = cl.getOptionValue("mqos");
        int mqttQos = 0;
        if (mqttQosOpt != null) {
            mqttQos = Integer.parseInt(mqttQosOpt);
        }

        // Mqtt User
        String mqttUsrOpt = cl.getOptionValue("mqusr");
        String mqttUsr = "user";
        if (mqttUsrOpt != null) {
            mqttUsr = mqttUsrOpt;
        }

        String mqttPswOpt = cl.getOptionValue("mqusr");
        String mqttPsw = "password";
        if (mqttPswOpt != null) {
            mqttPsw = mqttPswOpt;
        }

        Boolean cleanSession = false;

        // Get models folder
        String modelsFolderPath = cl.getOptionValue("m");

        // Get mDNS publish switch
        Boolean publishDNSSdServices = cl.hasOption("mdns");

        /*
         * TODO Parse configuration file in order to get -the Southboud interfaces to
         * use (MQTT, HTTP); -MQTT parameter (broker, clientID, Sub_Topic, Resources)
         */

        try {
            createAndStartVO(localAddress, localPort, brokerAddr, brokerPort, mqttQos, mqttUsr, mqttPsw, cleanSession,
                    modelsFolderPath, publishDNSSdServices, args);
        } catch (BindException e) {
            System.err
                    .println(String.format("Web port %s is already used, you could change it using 'webport' option."));
            formatter.printHelp(USAGE, options);
        } catch (Exception e) {
            LOG.error("Spring stopped with unexpected error ...", e);
        }

    }

    public static void createAndStartVO(String localAddress, int localPort, String brokerAddress, int brokerPort,
            int mqttqos, String userName, String password, Boolean cleanSession, String modelsFolderPath,
            Boolean publishDNSSdServices, String[] args) throws Exception {

        // Define model provider in order to aquire the objects defined into the
        // registration file
        // TODO registration aquire from xml
//        List<ObjectModel> models = ObjectLoader.loadDefault();
//        models.addAll(ObjectLoader.loadDdfResources("/models/", modelPaths));
//        if (modelsFolderPath != null) {
//        models.addAll(ObjectLoader.loadObjectsFromDir(new File(modelsFolderPath)));
//        }
//        // TODO set Model Provider
//        // LwM2mModelProvider modelProvider = new VersionedModelProvider(models);
//        // builder.setObjectModelProvider(modelProvider);
//
//        // Prepare MQTT
//        Gson gson = new Gson();
//
//        DeviceModel device = gson.fromJson(
//                new FileReader("C:\\Users\\Intel\\Desktop\\virtual-object\\registration.json"), DeviceModel.class);
//
//        // devEndp is the name of the physical device.
//        String devEndp = device.getEndpoint();
//
//        // clientId is the name of the digitalTwin
//        String clientId = device.getEndpoint() + "_VO";
//
//        if (clientId == null || clientId.equals("")) {
//            // clientId = "SampleJavaV3_"+action;
//            // clientId = devName + "_VO";
//            clientId = "deviceName" + "_VO";
//        }
//
//        // Register a service to DNS-SD
//        if (publishDNSSdServices) {
//
//            // Create a JmDNS instance
//            JmDNS jmdns = JmDNS.create(InetAddress.getLocalHost());
//
//            // Publish HTTP Service
//            ServiceInfo httpServiceInfo = ServiceInfo.create("_http._tcp.local.", "VO", 8080, "");
//            jmdns.registerService(httpServiceInfo);
//
//        }
//
        // // Start MQTT & Spring

        // // Default MQTT settings:
        // boolean quietMode = false; // whether debug should be printed to standard out
        // int qos = 0;
        // String broker = "m2m.eclipse.org";
        // int port = 1883;
        // String clientId = null;
        // String subTopic = "Sample/#";
        // String pubTopic = "Sample/Java/v3";
        // boolean cleanSession = false; // set to True for NON durable subscriptions
        // boolean ssl = false;
        // String userName = null;
        // String password = null;

//        String protocol = "tcp://";
//        String url = protocol + brokerAddress + ":" + brokerPort;

//        MqClient mqttClient = null;
//
//        try {
//            mqttClient = new MqClient(url, device.getEndpoint(), cleanSession, userName, password);
//        } catch (MqttException me) {
//            // Display full details of any exception that occurs
//            LOG.error("reason " + me.getReasonCode());
//            LOG.error("msg " + me.getMessage());
//            LOG.error("loc " + me.getLocalizedMessage());
//            LOG.error("cause " + me.getCause());
//            LOG.error("System exit, excep " + me);
//            me.printStackTrace();
//            System.exit(1);
//        }
//        LOG.info("MQTT Client started");
//        LOG.info("Subscribing to topics....");
        // Subscribe to several topic
        // i.e. :
        // - cmnd/devEndp/#;
        // - stat/devEndp/#;
        // - tele/devEndp/#;

        // TODO get specific sub topic form configuration file

//        if (mqttClient != null){
//            try {
//                mqttClient.subscribe("cmnd/" + devEndp + "/" + "#", mqttqos);
//            } catch (Throwable e1) {
//                LOG.error("Error subscribing to cmnd/ topic..."+ e1);
//                e1.printStackTrace();
//            }
//            try {
//                mqttClient.subscribe("stat/" + devEndp + "/" + "#", mqttqos);
//            } catch (Throwable e) {
//                LOG.error("Error subscribing to stat/ topic..."+ e);
//                e.printStackTrace();
//            }
//            try {
//                mqttClient.subscribe("tele/" + devEndp + "/" + "#", mqttqos);
//            } catch (Throwable e) {
//                LOG.error("Error subscribing to tele/ topic..."+ e);
//                e.printStackTrace();
//            }
//        }
        SpringApplication server = new SpringApplication(voApplication.class);
        server.run(args);
    }

}


