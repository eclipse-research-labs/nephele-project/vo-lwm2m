package it.unirc.vo.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unirc.vo.model.Resource;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResourceRepository extends CrudRepository<Resource, Long> {
    Optional<Resource> findByResourceId(String resourceId);

    @EntityGraph(attributePaths = "observers")
    Optional<Resource> findOneWithObserversById(Long id);

    @Query("SELECT r FROM Resource AS r, Object AS o, Device AS d\n" +
            "WHERE r.resourceId = :r_id AND o.id = r.object.id AND o.objectId = :o_id AND o.instanceId = :i_id AND d.endpoint = :d_id AND d.id = o.device.id")
    Optional<Resource> findResourceByDeviceObjectInstanceResourceId(@Param("d_id") String deviceId,
                                                                    @Param("o_id") String objectId,
                                                                    @Param("i_id") String instanceId,
                                                                    @Param("r_id") String resourceId);

    @Query("SELECT r FROM Resource AS r, Object AS o, Device AS d\n" +
            "WHERE o.id = r.object.id AND o.objectId = :o_id AND o.instanceId = :i_id AND d.endpoint = :d_id AND d.id = o.device.id")
    List<Resource> findResourceByDeviceObjectInstanceId(@Param("d_id") String deviceId,
                                                        @Param("o_id") String objectId,
                                                        @Param("i_id") String instanceId);

    @Query("SELECT r FROM Resource AS r")
    List<Resource> findAllResources();
}
