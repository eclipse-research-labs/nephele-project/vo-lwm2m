package it.unirc.vo.dto;

import lombok.Data;

import java.util.List;

@Data
public class MqttPayloadDto {
    String tmstp;
    List<MqttEventDto> e;
}
