package it.unirc.vo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "resource")
public class Resource extends Observable {

    public enum Operations {
        NONE, R, W, RW, E, RE, WE, RWE;

        public boolean isReadable() {
            return this == R || this == RW || this == RE || this == RWE;
        }

        public boolean isWritable() {
            return this == W || this == RW || this == WE || this == RWE;
        }

        public boolean isExecutable() {
            return this == E || this == RE || this == WE || this == RWE;
        }
    }

    public enum Type {
        STRING, INTEGER, FLOAT, BOOLEAN, OPAQUE, TIME, OBJLNK
    }

    // TODO: RENDERE RESOURCE_ID E OBJECT_ID UN COMPOSITE UNIQUE KEY

    public String resourceId; //i.e. 5700
    public Operations operations; //"R"
    public String instanceType; // "single"
    public Boolean mandatory; //true
    public String type; //float
    public String rangeEnumeration; //""
    public String units;//"defined by unit resource"
    public String description;//Last of Current Measured Value from the Sensor"
    @OneToMany(mappedBy = "resource", cascade = CascadeType.ALL)
    @OrderBy("createdAt DESC")
    public List<Value> values; //list of values saved in DB
    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "id")
    @JsonIgnore
    public Object object; // ex 3303/0

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    public void addValue(Value value) {
        values.add(value);
    }
}
