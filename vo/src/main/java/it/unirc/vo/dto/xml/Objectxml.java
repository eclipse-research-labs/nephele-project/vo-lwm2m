package it.unirc.vo.dto.xml;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class Objectxml {
    String ObjectType;
    String Name;
    String Description1;
    Long ObjectID;
    String ObjectURN;
    String MultipleInstances;
    String Mandatory;
    List<Resourcexml> Resources;
    String Description2;
}
