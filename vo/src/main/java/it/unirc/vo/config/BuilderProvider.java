package it.unirc.vo.config;

import com.influxdb.spring.influx.InfluxDB2OkHttpClientBuilderProvider;
import com.influxdb.spring.influx.InfluxDB2Properties;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component ("builderProvider")
public class BuilderProvider implements InfluxDB2OkHttpClientBuilderProvider {

    protected final InfluxDB2Properties properties;

    @Autowired
    public BuilderProvider (InfluxDB2Properties properties){
        this.properties = properties;
    }


    @Override
    public OkHttpClient.Builder get() {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .readTimeout(properties.getReadTimeout())
                .writeTimeout(properties.getWriteTimeout())
                .connectTimeout(properties.getConnectTimeout())
                .addInterceptor(logging);

        return okHttpBuilder;
    }
}
