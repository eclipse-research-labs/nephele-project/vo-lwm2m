package it.unirc.vo.service;

import it.unirc.vo.model.Resource;

public interface ClientService {

    void addObserverToResource(String observerAddr, Resource resource);
    
}
