package it.unirc.vo.dto.observe;

import it.unirc.vo.dto.ResourceDto;
import lombok.Data;

@Data
public class ObserveResourceData {
    String ep;
    String res;
    ResourceDto val;
}
