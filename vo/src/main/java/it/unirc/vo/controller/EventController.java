package it.unirc.vo.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import it.unirc.vo.model.Event;
import it.unirc.vo.repository.EventRepository;
import it.unirc.vo.service.EventService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/events")
public class EventController {

    private final EventService eventService;
    private final EventRepository eventRepository;

    @Autowired
    public EventController(EventService eventService, EventRepository eventRepository) {
        this.eventService = eventService;
        this.eventRepository = eventRepository;
    }

    @GetMapping
    public List<Event> findAll() {
        return (List<Event>) eventRepository.findAll();
    }

    @GetMapping("/{id}")
    public Event find(@PathVariable("id") Long id) {
        return eventRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("" + id));
    }

    @PostMapping
    public String post(@RequestBody Event event) {
        return eventService.checkMessageType(event);
    }

}
