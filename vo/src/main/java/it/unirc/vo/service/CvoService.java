package it.unirc.vo.service;

import it.unirc.vo.model.Resource;
import it.unirc.vo.model.Observer;
import it.unirc.vo.repository.ObserverRepository;
import it.unirc.vo.repository.ResourceRepository;
import it.unirc.vo.selection.MQTTCondition;
import it.unirc.vo.service.ClientServiceM;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CvoService {
    private final ObserverRepository observerRepository;
    private final ResourceRepository resourceRepository;
    private final boolean composite;

    private ClientService clientService;

    @Autowired
    public CvoService(ObserverRepository observerRepository, ResourceRepository resourceRepository, boolean composite) {
        this.observerRepository = observerRepository;
        this.resourceRepository = resourceRepository;
        this.composite = composite;
    }

    @Autowired(required = false)
	public void setClientServiceM(ClientServiceM clientServiceM) {
		this.clientService = clientServiceM;
	}

    
    @Autowired(required = false)
	public void setClientServiceU(ClientServiceU clientServiceU) {
		this.clientService = clientServiceU;
	}

    public String checkURL(String address) {
        String result = address;
        if (!address.startsWith("http")) {
            result = "http://" + address;
        }
        if (!address.contains(":")) {
            result = result + ":8080";
        }
        return result;
    }

    public ResponseEntity<String> registerCvo(HttpServletRequest request, String obsAddress) {
        String observerAddr;
        if (this.composite) {
            return new ResponseEntity<>("This is a CVO! Operation not avilable", HttpStatus.NOT_IMPLEMENTED);
        }
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        List<Resource> resources = resourceRepository.findAllResources();
        log.info("Registering CVO as Observer with IP: " + observerAddr);

        for (Resource resource : resources) {
            clientService.addObserverToResource(observerAddr, resource);
        }
        return new ResponseEntity<>("Success! The CVO is now registered as observer", HttpStatus.OK);

    }

    public ResponseEntity<String> deleteRegisterCvo(HttpServletRequest request, String obsAddress) {
        String observerAddr;
        if (this.composite) {
            return new ResponseEntity<>("This is a CVO! Operation not avilable", HttpStatus.NOT_IMPLEMENTED);
        }
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        List<Resource> resources = resourceRepository.findAllResources();
        log.info("De-Registering CVO as Observer with IP: " + observerAddr);
        for (Resource resource : resources) {
            resource.getObservers().stream()
                    .filter(observer -> observer.getAddress().equals(observerAddr)).findFirst()
                    .ifPresent(observer -> {
                        resource.getObservers().remove(observer);
                        observerRepository.delete(observer);
                    });
        }
        // observerRepository.findAll().forEach(observer -> {
        // if (observer.getAddress().equals(observerAddr)) {
        // resource.getObservers().remove(observer);
        // observerRepository.deleteById(observer.getId());
        // }
        // });
        return new ResponseEntity<>("Success! The CVO is not registered as observer anymore", HttpStatus.OK);
    }
}
