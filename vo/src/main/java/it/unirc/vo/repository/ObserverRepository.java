package it.unirc.vo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.unirc.vo.model.Observer;

@Repository
public interface ObserverRepository extends CrudRepository<Observer, Long> {
     @Query("SELECT new java.lang.Boolean(count(*) > 0) FROM Observer WHERE address = ?1 AND observable.id = ?2") 
     boolean existsByAddressAndObservable_Id(String address, Long observableId); //da rimappare

     @Query("SELECT new java.lang.Boolean(count(*) > 0) FROM Observer WHERE observable.id = ?1")
     boolean existsByObservable_Id(Long observableId); //da rimappare
}
