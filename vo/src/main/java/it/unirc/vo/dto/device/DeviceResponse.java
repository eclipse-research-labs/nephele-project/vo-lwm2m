package it.unirc.vo.dto.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class DeviceResponse {

	@JsonProperty("endpoint")
	private String endpoint;

	@JsonProperty("address")
	private String address;

	@JsonProperty("additionalRegistrationAttributes")
	private AdditionalRegistrationAttributes additionalRegistrationAttributes;

	@JsonProperty("bindingMode")
	private String bindingMode;

	@JsonProperty("objectLinks")
	private List<ObjectLinksItem> objectLinks;

	@JsonProperty("Version")
	private String version;

	@JsonProperty("lastUpdate")
	private String lastUpdate;

	@JsonProperty("registrationId")
	private String registrationId;

	@JsonProperty("registrationDate")
	private String registrationDate;

	@JsonProperty("lifetime")
	private long lifetime;

	@JsonProperty("rootPath")
	private String rootPath;

	@JsonProperty("secure")
	private boolean secure;
}
