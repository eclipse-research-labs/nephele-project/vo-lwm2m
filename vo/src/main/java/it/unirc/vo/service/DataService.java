package it.unirc.vo.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.unirc.vo.dto.ResourceDto;
import it.unirc.vo.model.Value;
import it.unirc.vo.repository.InfluxFunctions;
import it.unirc.vo.repository.ResourceRepository;
import it.unirc.vo.repository.ValueRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class DataService {
    private final ResourceRepository resourceRepository;
    private final ValueRepository valueRepository;

    private final InfluxFunctions influxFunctions;

    @Autowired
    public DataService(ResourceRepository resourceRepository, ValueRepository valueRepository,
            InfluxFunctions influxFunctions) {
        this.resourceRepository = resourceRepository;
        this.valueRepository = valueRepository;
        this.influxFunctions = influxFunctions;
    }

    public List<ResourceDto> findValuesByResource(String deviceId, String objectId, String instanceId,
            String resourceId, String value, Integer operator) {
        List<ResourceDto> resourceDtos = new ArrayList<>();

        resourceRepository.findResourceByDeviceObjectInstanceResourceId(deviceId, objectId, instanceId, resourceId)
                .ifPresent(resource -> {
                    List<Value> values = new ArrayList<>();
                    if (operator == null) {
                        values = valueRepository.findByResourceIdAndValueOrderByCreatedAtDesc(resource.getId(), value);
                        // values = influxFunctions.queryAllDataMod(resource.getId().toString());
                    } else if (operator == 0) {
                        values = valueRepository.findByResourceIdAndValueMoreThanOrderByCreatedAtDesc(resource.getId(),
                                value);
                    } else if (operator == 1) {
                        values = valueRepository.findByResourceIdAndValueLessThanOrderByCreatedAtDesc(resource.getId(),
                                value);
                    }
                    values.forEach(value1 -> {
                        ResourceDto resourceDto = new ResourceDto();
                        resourceDto.setId(resource.getResourceId());
                        resourceDto.setValue(value1.getValue());
                        resourceDto.setTimestamp(value1.getCreatedAt().toString());
                        resourceDtos.add(resourceDto);
                    });
                });
        return resourceDtos;
    }

    public List<ResourceDto> findValuesByResourceLimit(String deviceId, String objectId, String instanceId,
            String resourceId, Integer limit) {
        List<ResourceDto> resourceDtos = new ArrayList<>();

        resourceRepository.findResourceByDeviceObjectInstanceResourceId(deviceId, objectId, instanceId, resourceId)
                .ifPresent(resource -> {
                    Pageable pageable = PageRequest.of(0, limit, Sort.by("createdAt").descending());
                    List<Value> values = valueRepository.findAllByResourceId(resource.getId(), pageable);
                    values.forEach(value1 -> {
                        ResourceDto resourceDto = new ResourceDto();
                        resourceDto.setId(resource.getResourceId());
                        resourceDto.setValue(value1.getValue());
                        resourceDto.setTimestamp(value1.getCreatedAt().toString());
                        resourceDtos.add(resourceDto);
                    });
                });
        return resourceDtos;
    }

    public List<ResourceDto> findValuesByResourceAndDate(String deviceId, String objectId, String instanceId,
            String resourceId, String startString, String endString) throws ParseException {
        List<ResourceDto> resourceDtos = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Date startDate = dateFormat.parse(startString);
        Date endDate = dateFormat.parse(endString);

        resourceRepository.findResourceByDeviceObjectInstanceResourceId(deviceId, objectId, instanceId, resourceId)
                .ifPresent(resource -> {
                    List<Value> values = valueRepository.findByResourceIdAndCreatedAtBetweenOrderByCreatedAtDesc(
                            resource.getId(), startDate, endDate);
                    values.forEach(value1 -> {
                        ResourceDto resourceDto = new ResourceDto();
                        resourceDto.setId(resource.getResourceId());
                        resourceDto.setValue(value1.getValue());
                        resourceDto.setTimestamp(value1.getCreatedAt().toString());
                        resourceDtos.add(resourceDto);
                    });
                });
        return resourceDtos;
    }
}
