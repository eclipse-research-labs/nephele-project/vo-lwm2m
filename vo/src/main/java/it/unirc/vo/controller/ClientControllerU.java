package it.unirc.vo.controller;

//import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import it.unirc.vo.dto.*;
import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.dto.observe.ObserveObjectResponse;
import it.unirc.vo.dto.observe.ObserveResourceResponse;

import it.unirc.vo.service.ClientServiceU;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;

/* Nuovi import */
/* ***************************************************************************************************************************************** */
/* ***************************************************************************************************************************************** */
/* ***************************************************************************************************************************************** */

import org.springframework.context.annotation.Conditional;
import it.unirc.vo.selection.CoAPCondition;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import it.unirc.vo.service.ClientServiceU;

/* ----------------------------------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------------------------- */

@Log4j2
@RestController
@RequestMapping("/api/clients")
@Conditional(CoAPCondition.class)
public class ClientControllerU {

    private final ClientServiceU clientServiceU;

    @Autowired
    public ClientControllerU(ClientServiceU clientServiceU) {
        this.clientServiceU = clientServiceU;
    }

    @GetMapping("/{endpoint}")
    public DeviceResponse getDeviceResponse(
            @PathVariable("endpoint") String endpoint) {
        return clientServiceU.getDeviceResponse(endpoint);
    }

    @GetMapping("/{endpoint}/{objectId}")
    public ObjectResponseDto getObjectResponse(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            HttpServletRequest request,
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice ********* */
        return clientServiceU.getObjectResponse(endpoint, objectId, request, response); /* Nuovo codice ***** */
    }

    @GetMapping("/{endpoint}/{objectId}/{instanceId}")
    public ObjectInstanceResponseDto getObjectInstanceResponse(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            HttpServletRequest request, 
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice ******/
        return clientServiceU.getObjectInstanceResponse(endpoint, objectId, instanceId, request, response); /* Nuovo codice ******/
    }

    @GetMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public ResourceResponseDto getResourceResponse(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestParam(required = false) boolean getRealtime,
            HttpServletRequest request, 
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice ******/
        return clientServiceU.getResourceResponse(endpoint, objectId, instanceId, resourceId, request, response, getRealtime); /* Nuovo codice *********/
    }

    @PostMapping("/{endpoint}/{objectId}/{instanceId}/observe")
    public ObserveObjectResponse observeObjectInstance(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request,
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice ******/
        return clientServiceU.observeObjectInstance(endpoint, objectId, instanceId, obsAddr, request, response); /* Nuovo codice ******/
    }

    @PostMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}/observe")
    public ObserveResourceResponse observeResource(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request,
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice ************/
        return clientServiceU.observerResource(endpoint, objectId, instanceId, resourceId,obsAddr, request, response); /* Nuovo codice ********/
    }


    @DeleteMapping("/{endpoint}/{objectId}/{instanceId}/observe")
    public ResponseEntity<String> deleteObserveObjectInstance(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request,
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice  */
        clientServiceU.deleteObserveObjectInstance(endpoint, objectId, instanceId, obsAddr, request, response); /* Nuovo codice */
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}/observe")
    public ResponseEntity<String> deleteObserveResource(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            @RequestBody(required = false) String obsAddr,
            HttpServletRequest request,
            HttpServletResponse response) throws InterruptedException, IOException { /* Nuovo codice */
        clientServiceU.deleteObserverResource(endpoint, objectId, instanceId, resourceId, obsAddr, request, response); /* Nuovo codice */
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{endpoint}/{objectId}/{instanceId}")
    public ResponseEntity<String> writeObjectInstance(
            @PathVariable("endpoint") String endpoint,  
            @PathVariable("objectId") String objectId,                                     
            @PathVariable("instanceId") String instanceId,
            @RequestBody ObjectInstanceDto objectInstanceDto,
            HttpServletRequest request,
            HttpServletResponse response) throws Throwable { /* Nuovo codice  */
        clientServiceU.writeObjectInstance(endpoint, objectId, instanceId, objectInstanceDto, request, response); /* Nuovo codice */
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PutMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public ResponseEntity<String> writeResource(
            @PathVariable("endpoint") String endpoint,             
            @PathVariable("objectId") String objectId,            
            @PathVariable("instanceId") String instanceId,             
            @PathVariable("resourceId") String resourceId,          
            @RequestBody ResourceDto resourceDto,
            HttpServletRequest request,
            HttpServletResponse response) throws Throwable { /* Nuovo codice */
        clientServiceU.writeResource(endpoint, objectId, instanceId, resourceId, resourceDto, request, response); /* Nuovo codice */
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/{endpoint}/{objectId}/{instanceId}/{resourceId}")
    public ResponseEntity<String> executeCommand(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            @PathVariable("instanceId") String instanceId,
            @PathVariable("resourceId") String resourceId,
            HttpServletRequest request,
            HttpServletResponse response) throws Throwable { /* Nuovo codice  */
        clientServiceU.executeCommand(endpoint, objectId, instanceId, resourceId, request, response); /* Nuovo codice  */
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    /* Nuovo codice */
    /*
     * *****************************************************************************
     * ************************************************************
     */
    /*
     * *****************************************************************************
     * ************************************************************
     */
    /*
     * *****************************************************************************
     * ************************************************************
     */

    @DeleteMapping("/{endpoint}")
    public ResponseEntity<String> deleteObject(
            @PathVariable("endpoint") String endpoint,
            HttpServletRequest request) {
        clientServiceU.deleteObject(endpoint, request);
        return new ResponseEntity<>("Deleted :" + endpoint, HttpStatus.OK);
    }

    @DeleteMapping("/{endpoint}/{objectId}")
    public ResponseEntity<String> deleteInstance(
            @PathVariable("endpoint") String endpoint,
            @PathVariable("objectId") String objectId,
            HttpServletRequest request) {
        clientServiceU.deleteInstance(endpoint, objectId, request);
        return new ResponseEntity<>("Deleted :" + objectId, HttpStatus.OK);
    }

    /*
     * -----------------------------------------------------------------------------
     * ------------------------------------------------------------
     */
    /*
     * -----------------------------------------------------------------------------
     * ------------------------------------------------------------
     */
    /*
     * -----------------------------------------------------------------------------
     * ------------------------------------------------------------
     */

}
