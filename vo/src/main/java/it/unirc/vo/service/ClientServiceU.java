package it.unirc.vo.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import it.unirc.vo.coap.CoapClient;
import it.unirc.vo.coap.JSONConv;
import it.unirc.vo.coap.JacksonLwM2mNodeDeserializer;
import it.unirc.vo.coap.JacksonLwM2mNodeSerializer;
import it.unirc.vo.coap.JacksonRegistrationSerializer;
import it.unirc.vo.dto.*;
import it.unirc.vo.dto.device.Attributes;
import it.unirc.vo.dto.device.DeviceResponse;
import it.unirc.vo.dto.device.ObjectLinksItem;
import it.unirc.vo.dto.observe.*;
import it.unirc.vo.model.Object;
import it.unirc.vo.model.Observer;
import it.unirc.vo.model.Resource;
import it.unirc.vo.model.Value;
import it.unirc.vo.model.ValueInflux;
import it.unirc.vo.mqtt.MqClient;
import it.unirc.vo.repository.*;
import it.unirc.vo.selection.CoAPCondition;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.leshan.core.node.LwM2mNode;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.node.LwM2mSingleResource;
import org.eclipse.leshan.core.request.ContentFormat;
import org.eclipse.leshan.core.request.ExecuteRequest;
import org.eclipse.leshan.core.request.ObserveRequest;
import org.eclipse.leshan.core.request.ReadRequest;
import org.eclipse.leshan.core.request.WriteRequest;
import org.eclipse.leshan.core.request.WriteRequest.Mode;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.LwM2mResponse;
import org.eclipse.leshan.core.response.ObserveResponse;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;
import org.eclipse.leshan.server.registration.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service
@Conditional(CoAPCondition.class)
@Slf4j
public class ClientServiceU implements ClientService {
    private final DeviceRepository deviceRepository;
    private final ObjectRepository objectRepository;
    private final ResourceRepository resourceRepository;
    private final ObserverRepository observerRepository;
    private final CoapClient coapClient;
    private final ObjectMapper objectMapper;
    private final boolean composite;

    private final InfluxFunctions influxFunctions;

    @Autowired
    public ClientServiceU(DeviceRepository deviceRepository, ObjectRepository objectRepository,
            ResourceRepository resourceRepository, ObserverRepository observerRepository,
            CoapClient coapClient, ObjectMapper objectMapper, boolean composite, 
            InfluxFunctions influxFunctions) {
        this.deviceRepository = deviceRepository;
        this.objectRepository = objectRepository;
        this.resourceRepository = resourceRepository;
        this.observerRepository = observerRepository;

        this.coapClient = coapClient;
        this.objectMapper = objectMapper;
        this.composite = composite;
        this.influxFunctions = influxFunctions;

        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        SimpleModule module = new SimpleModule();
        module.addSerializer(LwM2mResponse.class, new it.unirc.vo.coap.JacksonResponseSerializer());
        objectMapper.registerModule(module);
        module.addSerializer(Registration.class, new JacksonRegistrationSerializer(coapClient.getPresenceService()));
        module.addSerializer(LwM2mNode.class, new JacksonLwM2mNodeSerializer());
        module.addDeserializer(LwM2mNode.class, new JacksonLwM2mNodeDeserializer());
    }

    public DeviceResponse getDeviceResponse(String endpoint) {
        DeviceResponse response = new DeviceResponse();
        deviceRepository.findByEndpoint(endpoint).ifPresent(e -> {
            response.setEndpoint(e.getEndpoint());
            response.setAddress(e.getAddress());
            response.setBindingMode(e.getBindingMode());
            List<ObjectLinksItem> objectLinks = new ArrayList<>();
            // objectLinks.add(new ObjectLinksItem("/", new
            // Attributes(e.getResourceType())));
            e.getObjectLinks().forEach(o -> {
                objectLinks.add(new ObjectLinksItem("/" + o.getObjectId() + "/" + o.getInstanceId(),
                        new Attributes(e.getResourceType())));
            });
            response.setObjectLinks(objectLinks);
            response.setVersion(e.getLwM2mVersion());
            response.setLastUpdate(e.getUpdatedAt().toString());
            response.setRegistrationId(e.getRegistrationId());
            response.setRegistrationDate(e.getCreatedAt().toString());
            response.setLifetime(e.getLifetime());
            response.setRootPath(e.getRootPath());
            response.setSecure(false);
        });
        return response;
    }

    public ObjectResponseDto getObjectResponse(String endpoint, String objectId, HttpServletRequest req, HttpServletResponse resp) throws InterruptedException, IOException {
        ObjectResponseDto response = new ObjectResponseDto();
        response.setStatus("CONTENT");
        ContentObjectDto content = new ContentObjectDto();
        content.setId(objectId);
        List<ObjectInstanceDto> objectInstanceDtos = new ArrayList<>();

        /* ----------- COAP ------------------------------- */
        if(req.getParameter("getRealtime") != null) {

            String r = endpoint + "/" + objectId;
            String [] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];
            Registration registration = coapClient.getRegistrationService().getByEndpoint (clientEndpoint);

            if(registration != null) {

                // get content format
                String contentFormatParam = req.getParameter("format");
                ContentFormat contentFormat = contentFormatParam != null ? ContentFormat.fromName(contentFormatParam.toUpperCase()) : null;

                // create & process request
                ReadRequest request = new ReadRequest(contentFormat, Integer.valueOf(objectId));
                ReadResponse cResponse = coapClient.send(registration, request);
                String respon = this.objectMapper.writeValueAsString(cResponse);
                String [][] payload = JSONConv.parser(respon,"object");

                objectRepository.findByObjectId(objectId).forEach(object -> {

                    ObjectInstanceDto objectInstanceDto = new ObjectInstanceDto();
                    objectInstanceDto.setId(object.getInstanceId());
                    List <ResourceDto> resourceDtos = new ArrayList<>();
                    List <Resource> resource = new ArrayList<>();
                    resource = object.getResources();
                    int[] lis = new int[resource.size()];
                    int k=0;
                    int l=k+2;

                    for(int j=0;j<resource.size();j++){
                        lis[j]=(Integer.parseInt(resource.get(j).getResourceId()));
                    }

                    while(k< resource.size())  {

                        ResourceDto resourceDto = new ResourceDto();
                        String a="";
                        String b="";
                        Arrays.sort(lis);
                        a = String.valueOf(lis[k]);

                        if(l<payload.length) {
                            b = payload[l][0];
                        }

                        if(a.equals(b)) {
                            resourceDto.setId(payload[l][0]);
                            resourceDto.setValue(payload[l][1]);
                            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                            LocalDateTime now = LocalDateTime.now();
                            resourceDto.setTimestamp(dtf.format(now));

                            resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, object.getInstanceId(), payload[l][0]).ifPresent(resource1 -> {

                                influxFunctions.writePointValue(resource1.id, resourceDto.getValue(), resource1.type);
   
                            });

                            k++;
                            l++;

                        } else {

                            resourceDto.setId(resource.get(k).getResourceId());

                            if (resource.get(k).getValues().size() != 0) {

                                resourceDto.setValue(resource.get(k).getValues().get(0).getValue());
                                resourceDto.setTimestamp(resource.get(k).getValues().get(0).getCreatedAt().toString());

                            }

                            k++;

                        }

                        resourceDtos.add(resourceDto);

                    }

                    objectInstanceDto.setResources(resourceDtos);
                    objectInstanceDtos.add(objectInstanceDto);

                });

                content.setInstances(objectInstanceDtos);
                response.setContent(content);
                return response;

            } else {

                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().format("No registered client with id '%s'", clientEndpoint).flush();
                response=null;
                return response;

            }

        } else {
        /* ----------- COAP end ------------------------------- */

            objectRepository.findByEndpointObjectId(endpoint, objectId).forEach(object -> {
                ObjectInstanceDto objectInstanceDto = new ObjectInstanceDto();
                objectInstanceDto.setId(object.getInstanceId());
                List<ResourceDto> resourceDtos = new ArrayList<>();
                object.getResources().forEach(resource -> {
                    ResourceDto resourceDto = new ResourceDto();
                    resourceDto.setId(resource.getResourceId());
                    if (resource.getValues().size() != 0) {
                        resourceDto.setValue(resource.getValues().get(0).getValue());
                        resourceDto.setTimestamp(resource.getValues().get(0).getCreatedAt().toString());
                    }
                    resourceDtos.add(resourceDto);
                });
                objectInstanceDto.setResources(resourceDtos);
                objectInstanceDtos.add(objectInstanceDto);
            });
            content.setInstances(objectInstanceDtos);
            response.setContent(content);
            return response;
        }
    }

    public ObjectInstanceResponseDto getObjectInstanceResponse(String endpoint, String objectId, String instanceId, HttpServletRequest req, HttpServletResponse resp) throws InterruptedException, IOException {

        ObjectInstanceResponseDto response = new ObjectInstanceResponseDto();
        response.setStatus("CONTENT");
        ContentResourceDto content = new ContentResourceDto();
        content.setId(objectId + "/" + instanceId);
        List<ResourceDto> resourceDtos = new ArrayList<>();

        /* ---------------------- COAP ----------------------------------- */

        Object object1 = new Object();
        boolean empty=true;
        object1 =  objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).get();
        int c = 0;

        List<Resource> resources = new ArrayList<>();
        resources =  object1.getResources();
        while(c< resources.size()) {
            if (resources.get(c).getValues().size() != 0) {
                empty=false;
            }
            c++;
        }

        if (req.getParameter("getRealtime")!=null || empty==true) {

            String r = endpoint + "/" + objectId + "/" + instanceId;
            String[] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];
            Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);
            
            if (registration != null) {

                String contentFormatParam = req.getParameter("format");
                ContentFormat contentFormat = contentFormatParam != null ? ContentFormat.fromName(contentFormatParam.toUpperCase()) : null;
                ReadRequest request = new ReadRequest(contentFormat, Integer.valueOf(objectId), Integer.valueOf(instanceId));
                ReadResponse cResponse = coapClient.send(registration, request);
                String respon = this.objectMapper.writeValueAsString(cResponse);
                String[][] payload = JSONConv.parser(respon,"instance");

                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {

                    List<Resource> resource = new ArrayList<>();
                    resource =  object.getResources();

                    int k=0;
                    int l=k+2;

                    while(k< resource.size()) {

                        ResourceDto resourceDto = new ResourceDto();
                        String a="";
                        String b="";
                        a = resource.get(k).getResourceId();

                        if(l<payload.length) {
                            b = payload[l][0];
                        }

                        if(a.equals(b)) {
                            resourceDto.setId(payload[l][0]);
                            resourceDto.setValue(payload[l][1]);
                            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                            LocalDateTime now = LocalDateTime.now();
                            resourceDto.setTimestamp(dtf.format(now));

                            resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, object.getInstanceId(), payload[l][0]).ifPresent(resource1 -> {

                               influxFunctions.writePointValue(resource1.id, resourceDto.getValue(), resource1.type);
   

                            });

                            k++;
                            l++;

                        } else {

                            resourceDto.setId(resource.get(k).getResourceId());

                            if(resource.get(k).getValues().size() != 0) {
                                resourceDto.setValue(resource.get(k).getValues().get(0).getValue());
                                resourceDto.setTimestamp(resource.get(k).getValues().get(0).getCreatedAt().toString());
                            }
                            k++;

                        }

                        resourceDtos.add(resourceDto);
                    }

                });

                content.setResources(resourceDtos);
                response.setContent(content);
                return response;

            } else {

                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().format("No registered client with id '%s'", clientEndpoint).flush();
                response=null;
                return response;
            }

        } else {


            /* ---------------------- COAP end ------------------------------- */


            objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
                object.getResources().forEach(resource -> {
                    ResourceDto resourceDto = new ResourceDto();
                    resourceDto.setId(resource.getResourceId());
                    if (resource.getValues().size() != 0) {
                        resourceDto.setValue(resource.getValues().get(0).getValue());
                        resourceDto.setTimestamp(resource.getValues().get(0).getCreatedAt().toString());
                    } else {
                        this.addObserverToObjectInstanceOneShot(req, object);
                    }
                    resourceDtos.add(resourceDto);
                });
            });
            content.setResources(resourceDtos);
            response.setContent(content);
            return response;
        }
    }

    /* THIS METHOD USES INFLUX DB */
    public ResourceResponseDto getResourceResponse(String endpoint, 
        String objectId, String instanceId, 
        String resourceId, HttpServletRequest req, 
        HttpServletResponse resp, boolean isRealtime) 
            throws InterruptedException, IOException {
        
        /* ---------------------- COAP ------------------------------- */

        Resource resource1 = resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId,instanceId,resourceId).get();
        boolean empty=true;

        if (resource1.getValues().size() != 0) {
            empty=false;
        }

        if (req.getParameter("getRealtime")!=null || empty==true) {

            String r = endpoint + "/" + objectId + "/" + instanceId;
            String[] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];
            Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

            if (registration != null) {

                // get content format
                String contentFormatParam = req.getParameter("format");
                ContentFormat contentFormat = contentFormatParam != null ? ContentFormat.fromName(contentFormatParam.toUpperCase()) :null;
                ReadRequest request = new ReadRequest(contentFormat, Integer.valueOf(objectId), Integer.valueOf(instanceId), Integer.valueOf(resourceId));
                ReadResponse cResponse = coapClient.send(registration, request);
                ResourceResponseDto response = new ResourceResponseDto();

                if(cResponse.getContent()!=null) {

                    String respon = this.objectMapper.writeValueAsString(cResponse);
                    String[][] payload = JSONConv.parser(respon,"resource");
                    response.setStatus("CONTENT");
                    ResourceDto resourceDto = new ResourceDto();
                    resourceDto.setId(resourceId);
                    resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId).ifPresent(resource -> {

                        resourceDto.setId(payload[0][0]);
                        resourceDto.setValue(payload[0][1]);
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                        LocalDateTime now = LocalDateTime.now();
                        resourceDto.setTimestamp(dtf.format(now));
                        
                        influxFunctions.writePointValue(resource1.id, resourceDto.getValue(), resource1.type);
   

                    });

                    response.setContent(resourceDto);
                    return response;

                } else {

                    log.info("Cant' read from resource");
                    response=null;
                    return response;

                }

            } else {

                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                resp.getWriter().format("No registered client with id '%s'", clientEndpoint).flush();
                ResourceResponseDto response = new ResourceResponseDto();
                response=null;
                return response;

            }

        } else {

        /* ---------------------- COAP end ------------------------------- */
        
            ResourceResponseDto response = new ResourceResponseDto();
            response.setStatus("CONTENT");
            ResourceDto resourceDto = new ResourceDto();
            resourceDto.setId(resourceId);
            objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
                object.getResources().stream().filter(r -> r.getResourceId().equals(resourceId))
                        .findFirst()
                        .ifPresent(resource -> {
                            /*
                            * if (resource.getValues().size() != 0) {
                            * resourceDto.setValue(resource.getValues().get(0).getValue());
                            * resourceDto.setTimestamp(resource.getValues().get(0).getCreatedAt().toString(
                            * ));
                            * } else if (resource.getValues().size() == 0 || isRealtime) {
                            * this.addObserverToResourceOneShot(request, resource);
                            * }
                            *
                            */

                            List<ValueInflux> listOfValue = influxFunctions
                                    .queryAllDataMod(resource.getId().toString() /*, resource.getType()*/);
                            if (listOfValue.size() != 0) {
                                ValueInflux firstValue = listOfValue.get(0);
                                resourceDto.setValue(firstValue.getValue().toString());
                                log.info("FIRST value.getValue.tostring()" + firstValue.getValue().toString());
                                resourceDto.setTimestamp(firstValue.getTime().toString());
                            } else if (listOfValue.size() == 0 || isRealtime) {
                                this.addObserverToResourceOneShot(req, resource);
                            }

                        });
            });
            response.setContent(resourceDto);
            return response;
        }
    }

    public ObserveObjectResponse observeObjectInstance(String endpoint, String objectId, String instanceId, String obsAddress, HttpServletRequest request, HttpServletResponse res) throws IOException {
    
        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        log.info("New Observer Address: " + observerAddr + " for endpoint: " + endpoint + "/" + objectId
                + "/" + instanceId);
        if (this.composite) {
            ObserveObjectResponse response = new ObserveObjectResponse();
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
                    if (!observerRepository.existsByObservable_Id(object.getId())) {
                        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://" + device.getAddress())
                                .addConverterFactory(JacksonConverterFactory.create())
                                .client(httpClient.build())
                                .build();

                        CompositeService service = retrofit.create(CompositeService.class);
                        Call<ObserveObjectResponse> callSync = service.observeObjectInstance(endpoint, objectId,
                                instanceId);
                        try {
                            Response<ObserveObjectResponse> r = callSync.execute();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });

                response.setEvent("NOTIFICATION");
                ObserveObjectData data = new ObserveObjectData();
                data.setEp(endpoint);
                data.setRes("/" + objectId + "/" + instanceId);
                ObserveObjectVal val = new ObserveObjectVal();
                val.setId(instanceId);
                List<ResourceDto> resources = new ArrayList<>();
                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {
                    this.addObserverToObjectInstance(observerAddr, object);
                    object.getResources().forEach(r -> {
                        ResourceDto resourceDto = new ResourceDto();
                        resourceDto.setId(r.getResourceId());
                        if (r.getValues().size() != 0) {
                            resourceDto.setValue(r.getValues().get(0).getValue());
                            resourceDto.setTimestamp(r.getCreatedAt().toString());
                        }
                        resources.add(resourceDto);
                    });
                });
                val.setResources(resources);
                data.setVal(val);
                response.setData(data);
            });
            return response;
        } else {
            ObserveObjectResponse response = new ObserveObjectResponse();
            response.setEvent("NOTIFICATION");
            ObserveObjectData data = new ObserveObjectData();
            data.setEp(endpoint);
            data.setRes("/" + objectId + "/" + instanceId);
            ObserveObjectVal val = new ObserveObjectVal();
            val.setId(instanceId);
            
            
            /* Nuovo codice */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */

            List<ResourceDto> resourceDtos = new ArrayList<>();
            String r = endpoint + "/" + objectId + "/" + instanceId + "/observe";
            String[] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];

            if (path.length >= 3 && "observe".equals(path[path.length - 1])) {

                try {

                    log.info("observe");
                    String target = StringUtils.substringBetween(r, clientEndpoint, "/observe");
                    Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                    if (registration != null) {

                        // get content format
                        String contentFormatParam = request.getParameter("format");
                        ContentFormat contentFormat = contentFormatParam != null ? ContentFormat.fromName(contentFormatParam.toUpperCase()) : null;

                        // create & process request
                        ObserveRequest req = new ObserveRequest(contentFormat, target);
                        ObserveResponse cResponse = coapClient.send(registration, req);
                        String respon = this.objectMapper.writeValueAsString(cResponse);
                        String[][] payload = JSONConv.parser(respon,"instance");


                        objectRepository.findByObjectIdAndInstanceId(objectId, instanceId).ifPresent(object -> {

                            this.addObserverToObjectInstance(observerAddr, object);
                            List<Resource> resource = new ArrayList<>();
                            data.setEp(object.getDevice().getEndpoint()) ;
                            data.setRes(object.getDevice().getEndpoint() +"/"+object.getObjectId()+"/"+payload[1][0]);
                            resource =  object.getResources();
                            int k=0;
                            int l=k+2;

                            while(k< resource.size()) {

                                ResourceDto resourceDto = new ResourceDto();
                                String a="";
                                String b="";
                                a = resource.get(k).getResourceId();

                                if(l<payload.length) {
                                    b = payload[l][0];
                                }

                                if(a.equals(b)) {

                                    resourceDto.setId(payload[l][0]);
                                    resourceDto.setValue(payload[l][1]);
                                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                                    LocalDateTime now = LocalDateTime.now();
                                    resourceDto.setTimestamp(dtf.format(now));

                                    resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, object.getInstanceId(), payload[l][0]).ifPresent(resource1 -> {

                                        influxFunctions.writePointValue(resource1.id, resourceDto.getValue(), resource1.type);

                                    });

                                    k++;
                                    l++;

                                } else {

                                    resourceDto.setId(resource.get(k).getResourceId());
                                    k++;

                                }
                                resourceDtos.add(resourceDto);
                            }

                        });

                        val.setResources(resourceDtos);
                        data.setVal(val);
                        response.setData(data);

                    } else {

                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        res.getWriter().format("no registered client with id '%s'", clientEndpoint).flush();

                    }

                } catch (RuntimeException | InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return response;

            /* ----------------------------------------------------------------------------------------------------------------------------------------- */
            /* ----------------------------------------------------------------------------------------------------------------------------------------- */
            /* ----------------------------------------------------------------------------------------------------------------------------------------- */

        }
    }

    public String checkURL(String address) {
        String result = address;
        if (!address.startsWith("http")) {
            result = "http://" + address;
        }
        if (!address.contains(":")) {
            result = result + ":8080";
        }
        return result;
    }

    public ObserveResourceResponse observerResource(String endpoint, String objectId, String instanceId, String resourceId, String obsAddress, HttpServletRequest request, HttpServletResponse res) throws IOException {
    
        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);

        }

        log.info("New Observer Address: " + observerAddr + " for endpoint: " + endpoint + "/" + objectId
                + "/" + instanceId + "/" + resourceId);
        if (this.composite) {
            ObserveResourceResponse response = new ObserveResourceResponse();
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                resourceRepository
                        .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                        .ifPresent(resource -> {
                            if (!observerRepository.existsByObservable_Id(resource.getId())) {
                                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl("http://" + device.getAddress())
                                        .addConverterFactory(JacksonConverterFactory.create())
                                        .client(httpClient.build())
                                        .build();

                                CompositeService service = retrofit.create(CompositeService.class);
                                Call<ObserveResourceResponse> callSync = service.observeResource(endpoint, objectId,
                                        instanceId, resourceId);
                                try {
                                    Response<ObserveResourceResponse> r = callSync.execute();
                                    ObserveResourceResponse observeResourceResponse = r.body();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                response.setEvent("NOTIFICATION");
                ObserveResourceData data = new ObserveResourceData();
                data.setEp(endpoint);
                data.setRes("/" + objectId + "/" + instanceId + "/" + resourceId);
                ResourceDto resourceDto = new ResourceDto();
                resourceDto.setId(resourceId);
                objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId)
                        .ifPresent(object -> {
                            object.getResources().stream().filter(r -> r.getResourceId().equals(resourceId))
                                    .findFirst()
                                    .ifPresent(resource -> {
                                        if (resource.getValues().size() != 0) {
                                            resourceDto.setValue(resource.getValues().get(0).getValue());
                                            resourceDto.setTimestamp(resource.getCreatedAt().toString());
                                        }
                                        this.addObserverToResource(observerAddr, resource);
                                    });
                        });
                data.setVal(resourceDto);
                response.setData(data);
            });
            return response;
        } else {
            ObserveResourceResponse response = new ObserveResourceResponse();
            response.setEvent("NOTIFICATION");
            ObserveResourceData data = new ObserveResourceData();
            data.setEp(endpoint);
            data.setRes("/" + objectId + "/" + instanceId + "/" + resourceId);
            ResourceDto resourceDto = new ResourceDto();
            resourceDto.setId(resourceId);

            /* Nuovo codice */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */


            String r = endpoint + "/" + objectId + "/" + instanceId +  "/" + resourceId + "/observe";
            String[] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];

            if (path.length >= 3 && "observe".equals(path[path.length - 1])) {

                try {

                    log.info("observe");
                    String target = StringUtils.substringBetween(r, clientEndpoint, "/observe");
                    Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                    if (registration != null) {
                        // get content format
                        String contentFormatParam = request.getParameter("format");
                        ContentFormat contentFormat = contentFormatParam != null ? ContentFormat.fromName(contentFormatParam.toUpperCase()) : null;
                        ObserveRequest req = new ObserveRequest(contentFormat, target);
                        ObserveResponse cResponse = coapClient.send(registration, req);
                        String respon = this.objectMapper.writeValueAsString(cResponse);
                        String[][] payload = JSONConv.parser(respon,"resource");
                        response.setEvent("NOTIFICATION");

                        objectRepository.findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId).ifPresent(object -> {

                            data.setEp(object.getDevice().getEndpoint()) ;
                            data.setRes(object.getDevice().getEndpoint() +"/"+object.getObjectId()+"/"+object.getInstanceId()+"/"+payload[0][0]);
                            resourceDto.setId(payload[0][0]);
                            resourceDto.setValue(payload[0][1]);
                            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                            LocalDateTime now = LocalDateTime.now();
                            resourceDto.setTimestamp(dtf.format(now));

                            resourceRepository.findResourceByDeviceObjectInstanceResourceId(data.getEp(), objectId, instanceId, resourceDto.getId()).ifPresent(resource -> {
                                this.addObserverToResource(observerAddr, resource);
                                
                               

                                /* ------ CONVERTING IN INFLUX ------- */
                                    /* 
                                    Value value = new Value();
                                    value.setValue(resourceDto.getValue());
                                    value.setResource(resource);
                                    valueRepository.save(value);
                                    */

                                    influxFunctions.writePointValue(resource.id, resourceDto.getValue(), resource.type);
                                    
                                    /* ------------------------------------ */

                            });

                        });

                        data.setVal(resourceDto);
                        response.setData(data);

                    } else {

                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        res.getWriter().format("no registered client with id '%s'", clientEndpoint).flush();

                    }

                } catch (RuntimeException | InterruptedException e) {
                    e.printStackTrace();
                }

            }

            return response;

        }

        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */

    }

    /* Nuovo codice */
    /* ***************************************************************************************************************************************** */
    /* ***************************************************************************************************************************************** */
    /* ***************************************************************************************************************************************** */

    public void deleteObject (String endpoint, HttpServletRequest request) {
        deviceRepository.delete(deviceRepository.findByEndpoint(endpoint).get());
    }

    public void deleteInstance (String endpoint, String objectId, HttpServletRequest request) {
        objectRepository.delete(objectRepository.findByObjectId(objectId).get(0));
    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */


    public void deleteObserveObjectInstance(String endpoint, String objectId, String instanceId, String obsAddress, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        log.info("Deleting Observer Address: " + observerAddr + "for Instance: " + endpoint + "/" + objectId + "/"
                + instanceId);
        objectRepository
                .findByEndpointObjectIdInstanceId(endpoint, objectId, instanceId)
                .ifPresent(object -> {
                    object.getObservers().stream()
                            .filter(observer -> observer.getAddress().equals(observerAddr)).findFirst()
                            .ifPresent(observer -> {
                                object.getObservers().remove(observer);
                                observerRepository.delete(observer);
                            });
                });
        
        /* Nuovo codice */        
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */

        String r = endpoint + "/" + objectId + "/" + instanceId + "/observe";
        String[] path = StringUtils.split(r, '/');
        String clientEndpoint = path[0];

        if (path.length >= 3 && "observe".equals(path[path.length - 1])) {

            try {

                String target = StringUtils.substringsBetween(r, clientEndpoint, "/observe")[0];
                Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                if (registration != null) {

                    coapClient.getObservationService().cancelObservations(registration, target);
                    log.info("Succesful Delete");
                    resp.setStatus(HttpServletResponse.SC_OK);

                } else {

                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().format("no registered client with id '%s'", clientEndpoint).flush();

                }

            } catch (RuntimeException e) {
                e.printStackTrace();
            }

            return;
        }

        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */


    }

    public void deleteObserverResource(String endpoint, String objectId, String instanceId, 
        String resourceId, String obsAddress, HttpServletRequest request, HttpServletResponse resp) throws IOException {


        String observerAddr;
        if (obsAddress == null) {
            observerAddr = request.getRemoteAddr();
        } else {
            observerAddr = checkURL(obsAddress);
        }
        log.info("Deleting Observer Address: " + observerAddr + "For Resource:" + endpoint + "/" + objectId + "/"
                + instanceId + "/" + resourceId);
        resourceRepository
                .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                .ifPresent(resource -> {
                    resource.getObservers().stream()
                            .filter(observer -> observer.getAddress().equals(observerAddr)).findFirst()
                            .ifPresent(observer -> {
                                resource.getObservers().remove(observer);
                                observerRepository.delete(observer);
                            });
                });

        /* Nuovo codice */
        /* ***************************************************************************************************************************************** */
        /* ***************************************************************************************************************************************** */
        /* ***************************************************************************************************************************************** */

        String r = endpoint + "/" + objectId + "/" + instanceId + "/" + resourceId + "/observe";
        String[] path = StringUtils.split(r, '/');
        String clientEndpoint = path[0];

        if (path.length >= 3 && "observe".equals(path[path.length - 1])) {

            try {

                String target = StringUtils.substringsBetween(r, clientEndpoint, "/observe")[0];
                Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                if (registration != null) {

                    coapClient.getObservationService().cancelObservations(registration, target);
                    log.info("Succesful Delete");
                    resp.setStatus(HttpServletResponse.SC_OK);

                } else {

                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().format("no registered client with id '%s'", clientEndpoint).flush();

                }

            } catch (RuntimeException e) {
                e.printStackTrace();
            }

            return;

        }

        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    }

    // void addObserverToResource(HttpServletRequest request, Resource resource) {
    public void addObserverToResource(String obsAddress, Resource resource) {

        if (observerRepository.existsByAddressAndObservable_Id(obsAddress, resource.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(obsAddress);
        observer.setObservable(resource);
        observerRepository.save(observer);
    }

    private void addObserverToObjectInstanceOneShot(HttpServletRequest request, Object object) {
        if (observerRepository.existsByAddressAndObservable_Id(request.getRemoteAddr(), object.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(request.getRemoteAddr());
        observer.setOneShot(true);
        observer.setObservable(object);
        observerRepository.save(observer);
    }

    private void addObserverToResourceOneShot(HttpServletRequest request, Resource resource) {
        if (observerRepository.existsByAddressAndObservable_Id(request.getRemoteAddr(), resource.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(request.getRemoteAddr());
        observer.setOneShot(true);
        observer.setObservable(resource);
        observerRepository.save(observer);
    }

    private void addObserverToObjectInstance(String obsAddress, Object object) {
        if (observerRepository.existsByAddressAndObservable_Id(obsAddress, object.getId()))
            return;
        Observer observer = new Observer();
        observer.setAddress(obsAddress);
        observer.setObservable(object);
        observerRepository.save(observer);
    }

    public void writeObjectInstance(String endpoint, String objectId, String instanceId, ObjectInstanceDto objectInstanceDto, HttpServletRequest req, HttpServletResponse resp) throws Throwable {
        /* Nuovo codice */
        /* ***************************************************************************************************************************************** */
        /* ***************************************************************************************************************************************** */
        /* ***************************************************************************************************************************************** */

        List <String> id = new ArrayList<>();
        List <String> value = new ArrayList<>();

        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */

        if (this.composite) {
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://" + device.getAddress())
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                CompositeService service = retrofit.create(CompositeService.class);
                Call<ResponseBody> callSync = service.putObjectInstance(endpoint, objectId, instanceId,
                        objectInstanceDto);
                try {
                    callSync.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else {
            /* Nuovo codice */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */

            objectInstanceDto.getResources().forEach(resourceDto -> {

                id.add(resourceDto.getId());
                value.add(resourceDto.getValue());

            });

            String r = endpoint + "/" + objectId + "/" + instanceId;
            String[] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];

            try {

                Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                if (registration != null) {

                    ContentFormat contentFormat = new ContentFormat("JSON", "JSON", 11543);
                    Collection<LwM2mResource> resources = new ArrayList<>();


                    for (int i=0; i<objectInstanceDto.getResources().size(); i++) {

                        resources.add(LwM2mSingleResource.newResource(Integer.valueOf(id.get(i)), value.get(i)));

                    }

                    WriteRequest request = new WriteRequest(Mode.REPLACE, contentFormat, Integer.valueOf(path[1]), Integer.valueOf(path[2]), resources);

                    WriteResponse cResponse = coapClient.send(registration, request);
                    String respon = this.objectMapper.writeValueAsString(cResponse);
                    log.info("Succesful Write: " + respon);
                    processDeviceResponse(req, resp, cResponse);

                    if(resp.getStatus()==204) {

                        for (int a=0; a<objectInstanceDto.getResources().size(); a++) {

                            String objId = path[1];
                            String instId = path[2];
                            
                            /* ------ CONVERTING IN INFLUX ------- 
                            
                            Value value1 = new Value();

                            ------------------------------------ */
                            String resId = id.get(a);
                            String v = value.get(a);

                            objectRepository.findByEndpointObjectIdInstanceId(endpoint, objId, instId).ifPresent(object1 -> {


                                String finalResID = resId;

                                object1.getResources().stream().filter(r1 -> r1.getResourceId().toString().equals(finalResID)).findFirst().ifPresent(resource1 -> {
                                    
                                    /* ------ CONVERTING IN INFLUX ------- */
                                    /* 
                                    value1.setValue(v);
                                    value1.setResource(resource1);
                                    valueRepository.save(value1);
                                    */

                                    influxFunctions.writePointValue(resource1.id, v, resource1.type);
                                    
                                    /* ------------------------------------ */

                                });
                            });
                        }
                    }

                } else {

                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().format("No registered client with id '%s'", clientEndpoint).flush();

                }

            } catch (RuntimeException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */
        /* ----------------------------------------------------------------------------------------------------------------------------------------- */

    }

    /* THIS METHOD USES INFLUX DB */
    public void writeResource(String endpoint, String objectId, String instanceId, String resourceId, ResourceDto resourceDto, HttpServletRequest req, HttpServletResponse resp) throws Throwable {
        if (this.composite) {
            deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://" + device.getAddress())
                        .addConverterFactory(JacksonConverterFactory.create())
                        .client(httpClient.build())
                        .build();

                CompositeService service = retrofit.create(CompositeService.class);
                Call<ResponseBody> callSync = service.putResource(endpoint, objectId, instanceId, resourceId,
                        resourceDto);
                try {
                    callSync.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                resourceRepository
                        .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                        .ifPresent(resource -> {

                            // CONVERTING TO VALUE INFLUX
                            //ValueInflux valueInflux = new ValueInflux();
                            //valueInflux.setResourceId(resource.id);
                            //valueInflux.setValue(resourceDto.getValue());
                            //influxFunctions.writeValue(valueInflux);


                            influxFunctions.writePointValue(resource.id, resourceDto.getValue(), resource.type);

                            /*
                             * Value value = new Value();
                             * value.setResource(resource);
                             * value.setValue(resourceDto.getValue());
                             * valueRepository.save(value);
                             */
                        });
            });
        } else {
            /* Nuovo codice */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */

            String r = endpoint + "/" + objectId + "/" + instanceId + "/" + resourceId;
            String[] path = StringUtils.split(r, '/');
            String clientEndpoint = path[0];
            
            try {

                Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                if (registration != null) {

                    ContentFormat contentFormat = new ContentFormat("JSON", "JSON", 11543);
                    WriteRequest request = new WriteRequest(contentFormat, Integer.valueOf(path[1]), Integer.valueOf(path[2]), Integer.valueOf(path[3]),resourceDto.getValue());
                    WriteResponse cResponse = coapClient.send(registration, request);
                    
                    
                    processDeviceResponse(req, resp, cResponse);

                    if(resp.getStatus()==204) {

                        String objId = path[1];
                        String instId = path[2];
                        
                        objectRepository.findByObjectIdAndInstanceId(objId, instId).ifPresent(object -> {

                            resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId).ifPresent(resource1 -> {
                                

                                /* ------ CONVERTING IN INFLUX ------- */
                                /*
                                Value value = new Value();
                                value.setResource(resource1);
                                value.setValue(resourceDto.getValue());
                                valueRepository.save(value);
                                */

                                influxFunctions.writePointValue(resource1.id, resourceDto.getValue(), resource1.type);

                                /* ------------------------------------ */
                                


                            });
                        });
                    }

                } else {

                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    resp.getWriter().format("No registered client with id '%s'", clientEndpoint).flush();

                }

            } catch (RuntimeException | InterruptedException e) {
                e.printStackTrace();
            }

            /* ----------------------------------------------------------------------------------------------------------------------------------------- */
            /* ----------------------------------------------------------------------------------------------------------------------------------------- */
            /* ----------------------------------------------------------------------------------------------------------------------------------------- */

        }
    }

    public void executeCommand(String endpoint, String objectId, String instanceId, String resourceId, HttpServletRequest req, HttpServletResponse resp) throws Throwable {
        if (this.composite) {
            resourceRepository
                    .findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId)
                    .ifPresent(resource -> {
                        deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {
                            this.addObserverToResourceOneShot(req, resource);
                            try {
                                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl("http://" + device.getAddress())
                                        .addConverterFactory(JacksonConverterFactory.create())
                                        .client(httpClient.build())
                                        .build();

                                CompositeService service = retrofit.create(CompositeService.class);
                                Call<ResponseBody> callSync = service.postCommand(endpoint, objectId, instanceId,
                                        resourceId);
                                callSync.execute();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        });
                    });
        } else {
             /* Nuovo codice */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */
            /* ***************************************************************************************************************************************** */

            resourceRepository.findResourceByDeviceObjectInstanceResourceId(endpoint, objectId, instanceId, resourceId).ifPresent(resource -> {

                deviceRepository.findByEndpoint(endpoint).ifPresent(device -> {

                    this.addObserverToResourceOneShot(req, resource);

                    try {

                        String r = endpoint + "/" + objectId + "/" + instanceId + "/" + resourceId;
                        String[] path = StringUtils.split(r, '/');
                        String clientEndpoint = path[0];

                        try {

                            Registration registration = coapClient.getRegistrationService().getByEndpoint(clientEndpoint);

                            if (registration != null) {

                                @SuppressWarnings("unused")
                                String params = null;

                                if (req.getContentLength() > 0) {
                                    params = IOUtils.toString(req.getInputStream(), StandardCharsets.UTF_8);
                                }

                                ExecuteRequest request = new ExecuteRequest(Integer.valueOf(path[1]), Integer.valueOf(path[2]), Integer.valueOf(path[3]), "0");
                                ExecuteResponse cResponse = coapClient.send(registration, request);
                                String respon = this.objectMapper.writeValueAsString(cResponse);
                                log.info("Succesful Execute: " +respon);
                                processDeviceResponse(req, resp, cResponse);

                            } else {

                                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                resp.getWriter().format("no registered client with id '%s'", clientEndpoint).flush();
                            }

                        } catch (RuntimeException | InterruptedException e) {
                            e.printStackTrace();
                        }

                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
            });

            /* ----------------------------------------------------------------------------------------------------------------------------------------- */
            /* ----------------------------------------------------------------------------------------------------------------------------------------- */
            /* ----------------------------------------------------------------------------------------------------------------------------------------- */

        }
    }

    
    /* Nuovo codice */
    /* ***************************************************************************************************************************************** */
    /* ***************************************************************************************************************************************** */
    /* ***************************************************************************************************************************************** */

    private void processDeviceResponse(HttpServletRequest req, HttpServletResponse resp, LwM2mResponse cResponse) throws IOException {

        if (cResponse == null) {

            log.warn(String.format("Request %s%s timed out.", req.getServletPath(), req.getPathInfo()));
            resp.setStatus(HttpServletResponse.SC_GATEWAY_TIMEOUT);
            resp.getWriter().append("Request timeout").flush();
        } else {
            String response = this.objectMapper.writeValueAsString(cResponse.getCode().getCode());
            resp.setContentType("application/json");
            resp.getOutputStream().write(response.getBytes());
            resp.setStatus(Integer.valueOf(response));
        }
    }

    /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */

}