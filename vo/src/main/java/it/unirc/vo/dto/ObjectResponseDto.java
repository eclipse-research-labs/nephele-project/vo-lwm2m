package it.unirc.vo.dto;

import lombok.Data;

@Data
public class ObjectResponseDto {
    String status;
    ContentObjectDto content;
}
