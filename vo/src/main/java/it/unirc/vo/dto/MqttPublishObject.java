package it.unirc.vo.dto;

import lombok.Data;

import java.util.List;

@Data
public class MqttPublishObject {
    List<MqttEventDto> e;
}
